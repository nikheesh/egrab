package com.egrab.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

public class ZoneList_Activity extends Activity {
	//declaration of variables

	
	ImageButton imgbtn_close;
	Intent intent;
	ListView zone_listview;
	ArrayAdapter<JsonObject> zoneAdapter;
	SharedPreferences pref;
	Editor editor;
	public  ProgressDialog proDialog;
	public  void startLoading(String msg) {
	    proDialog = new ProgressDialog(this);
	    proDialog.setMessage(msg);
	    proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	    proDialog.setCancelable(false);
	    proDialog.show();
	}

public  void stopLoading() {
	    proDialog.dismiss();
	    proDialog = null;
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
//		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_zonelist);
		
		 ApplicationClass appState = ((ApplicationClass)getApplicationContext());
	    	
			pref=appState.pref;
			editor=appState.editor;
		
		zoneAdapter = new ArrayAdapter<JsonObject>(this, 0) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if (convertView == null)
                    convertView = getLayoutInflater().inflate(R.layout.zonelist_item, null);

              
                final JsonObject json = getItem(position);

               
                LinearLayout ll=(LinearLayout)convertView.findViewById(R.id.ll_zonlist_layout);
               TextView txt_storename = (TextView)convertView.findViewById(R.id.txt_zonelist_zonename);
                txt_storename.setText(json.get("zoneName").getAsString());
                ll.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if(Utilities_Functions.checkinternet(getApplicationContext())){
						String zoneid=json.get("zoneID").getAsString();
						String custid=getIntent().getStringExtra("customerid");
						editor.putString("zonename",json.get("zoneName").getAsString());
						editor.putString("zone",zoneid);
						
						editor.commit();
						
				    	   updatezone(custid, zoneid);
						}
						else
						{
							Toast.makeText(getApplicationContext(), "Please check your internet connection", Toast.LENGTH_LONG).show();
						}
						
						
					}
				});
                
                
                
                return convertView;
            }
        };
		
		
		imgbtn_close=(ImageButton)findViewById(R.id.imgbtn_zonelist_close);
		
		zone_listview=(ListView)findViewById(R.id.listview_zonelist_zonelist);
		zone_listview.setAdapter(zoneAdapter);
		
	
	if(Utilities_Functions.checkinternet(getApplicationContext()))
	
		viewZones();
	else
		Toast.makeText(getApplicationContext(), "Please check your internet connection", Toast.LENGTH_LONG).show();
		
		
		
		
		
	}
	
	public void viewZones(){
		startLoading("Loading Availabe zones.Please wait");
		Ion.with(getApplicationContext())
		.load("http://egrab.qa/en/webservices/zone.php")
		.asJsonObject()
		.setCallback(new FutureCallback<JsonObject>() {
		   @Override
		    public void onCompleted(Exception e, JsonObject result) {
			   stopLoading();
			   if(e==null){
		       boolean status=result.get("success").getAsBoolean();
		       if(status){
		    	   JsonArray jsonarray=result.get("items").getAsJsonArray();
		    	   for (int i = 0; i < jsonarray.size(); i++) {
					JsonObject jsonobj=jsonarray.get(i).getAsJsonObject();
					zoneAdapter.add(jsonobj);
					
				}
		    	  
		       }
		       else{
		    	   //error dialog here;
		    	   Log.e(" not logged in", "sign up");
		       }
			   }
			   else{
					Toast.makeText(getApplicationContext(), "Please Check your internet connection", Toast.LENGTH_LONG).show();
				}
		    }
		});
		
	}
	
	public void updatezone(String custid,String zone){
		final String zoneid=zone;
		final String custid1=custid;
		Ion.with(getApplicationContext())
		.load("http://egrab.qa/en/webservices/updatezone.php?customerid="+custid+"&zone="+zone)
		.asJsonObject()
		.setCallback(new FutureCallback<JsonObject>() {
		   @Override
		    public void onCompleted(Exception e, JsonObject result) {
			   if(e==null){
		       boolean status=result.get("success").getAsBoolean();
		       if(status){
		    	  Toast.makeText(getApplicationContext(), "location changed", Toast.LENGTH_LONG).show();
				
		    	  if(Utilities_Functions.checkinternet(getApplicationContext())){
		    			String urlzone="http://egrab.qa/en/webservices/store.php?zone="+zoneid;
		    			Ion.with(getApplicationContext())
		    			.load(urlzone)
		    			.asJsonObject()
		    			.setCallback(new FutureCallback<JsonObject>() {
		    			   @Override
		    			    public void onCompleted(Exception e, JsonObject result) {
		    				   if(proDialog!=null)
		    					   stopLoading();
		    				   if(e==null){
		    			       boolean status=result.get("success").getAsBoolean();
		    			       if(status){
		    			    	   intent=new Intent(getApplicationContext(), StoresDetails_Activity.class);
		    						//	intent.putExtra("customerid", custid1);
		    						//	intent.putExtra("zone", zoneid);
		    			    	   editor.putString("storeid", "");
		    			    	   editor.commit();
		    							startActivity(intent);
		    							finish();
		    			       }
		    			       
		    			       else{
		    			    	   editor.putString("storeid", "11");
		    			    	   editor.commit();
		    	  	            Intent intent=new Intent(getApplicationContext(),Alert_Dialog_Activity.class);
		    	  	            startActivity(intent);
		    	  	            finish();
		    	  	            
		    			       }
		    			       
		    			      
		    				   }
		    				   else{
		    						Toast.makeText(getApplicationContext(), "Please Check your internet connection", Toast.LENGTH_LONG).show();
		    					}
		    				   
		    			    }
		    			});
		    			}
		    	  
		    	  
		    	  
		    	  
		    	  
		    	  
		    	  
		    	  
		    	
		    	  
		    	 // finish();
		    	  
		       }
		       else{
		    	   //error dialog here;
		    	   Log.e(" not logged in", "sign up");
		       }
		   }
		   else{
				Toast.makeText(getApplicationContext(), "Please Check your internet connection", Toast.LENGTH_LONG).show();
			}
			   
		    }
		});
		
		
	}
	
	
	public void closeDialog(View view){
			
		this.finish();
	}
	 @Override
	    public void onBackPressed() {
	       finish();
	    }


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}