package com.egrab.app;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DbHelper extends SQLiteOpenHelper {

	public DbHelper(Context applicationcontext) {
		super(applicationcontext, "eGrab.db", null, 4);
		Log.d("Database", "Created");
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		String query;
		query = "CREATE TABLE cartrequest (prodname STRING,brand STRING,qty STRING,desc STRING)";
		db.execSQL(query);
		Log.d("Table", "cartrequest Created");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		String query;
		query = "DROP TABLE IF EXISTS cartrequest";
		db.execSQL(query);
		onCreate(db);
	}

	public void insertProducts(String name, String brand, String qty,
			String desc) {
		SQLiteDatabase database = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("prodname", name);
		values.put("brand", brand);
		values.put("qty", qty);
		values.put("desc", desc);

		database.insert("cartrequest", null, values);
		database.close();
	}

	public ArrayList<HashMap<String, String>> getAllProducts() {
		ArrayList<HashMap<String, String>> wordList;
		wordList = new ArrayList<HashMap<String, String>>();
		String selectQuery = "SELECT  * FROM cartrequest";
		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("name", cursor.getString(0));
				map.put("category", cursor.getString(1));
				map.put("qty", cursor.getString(2));
				map.put("desc", cursor.getString(3));

				wordList.add(map);
			} while (cursor.moveToNext());
		}

		// return contact list
		return wordList;
	}

	public boolean deleteProduct(String id) {
		Log.d("delete product", "delete");
		SQLiteDatabase database = this.getWritableDatabase();
		return database.delete("cartrequest", "prodname" + "='" + id + "'", null) > 0;

		

	}
}
