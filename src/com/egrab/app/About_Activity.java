package com.egrab.app;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class About_Activity extends Activity{
	WebView txt_html;
	ImageButton img_back;
	
	ApplicationClass appState;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_about);
		
		txt_html=(WebView)findViewById(R.id.txt_about_abttxt);
		txt_html.loadUrl("file:///android_asset/about.html");
		
		img_back=(ImageButton)findViewById(R.id.imgbtn_help_backbtn);
		img_back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		
	}

}
