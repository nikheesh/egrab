package com.egrab.app;



import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;

//First page when app opens

public class MainActivity extends BaseActivity {

	//declaration of variables
	Button signin_btn,createnew_btn;
	Intent intent;
	
	private String[] navMenuTitles;
	private TypedArray navMenuIcons;
	private DrawerLayout mDrawerLayout;
	public boolean mSlideState=false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		 ApplicationClass appState = ((ApplicationClass)getApplicationContext());
	    	
		SharedPreferences pref=appState.pref;
		if(!pref.contains("userid")||!pref.contains("password")||!pref.contains("islogedin")){
		
//		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
				
		setContentView(R.layout.activity_main);
		
		getActionBar().hide();
		
		//access buttons here
		signin_btn=(Button)findViewById(R.id.btn_ativitymain_signin);
		createnew_btn=(Button) findViewById(R.id.btn_ativitymain_signup);
		
		//opens sign_in page
		signin_btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				intent=new Intent(getApplicationContext(),Login_Activity.class);
				startActivity(intent);
				
				
			}
		});
		
		
		//create new account
		createnew_btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				intent=new Intent(getApplicationContext(),CreateNew_Activity.class);
				startActivity(intent);
				
			}
		});
		
		}
		
		else{
			Intent i=new Intent(getApplicationContext(),StoresDetails_Activity.class);
			//i.putExtra("customerid", pref.getString("customerid", "0"));
		//	i.putExtra("zone", pref.getString("zone", "0"));
			i.putExtra("mode", "secondlogin");
			startActivity(i);
			finish();			
		}
		
		navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);
		navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);
		
		set(navMenuTitles,navMenuIcons);
		
		mSlideState = false;
		
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		
		mDrawerLayout.setDrawerListener(new ActionBarDrawerToggle(this,mDrawerLayout, R.drawable.ic_launcher, 0, 0){
			
			@Override
			public void onDrawerClosed(View drawerView) {                       
			    super.onDrawerClosed(drawerView);
			    mSlideState=false;//is Closed
			}
			
			@Override
			public void onDrawerOpened(View drawerView) {                       
			    super.onDrawerOpened(drawerView);
			    mSlideState=true;//is Opened
			}});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
}
