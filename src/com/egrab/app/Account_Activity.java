package com.egrab.app;



import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class Account_Activity extends Activity{
	TextView txt_email,txt_terms;
	ImageView img_terms;
	ImageButton img_back;
	ApplicationClass appState;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_account);
		
		txt_email=(TextView)findViewById(R.id.txt_acct_email);
		txt_terms=(TextView)findViewById(R.id.txt_acct_termsandcondition);
		txt_terms.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),Terms_Activity.class);
				startActivity(i);
			}
		});
		img_terms=(ImageView)findViewById(R.id.img_acct_terms);
		appState = ((ApplicationClass)getApplicationContext());
		String email=appState.pref.getString("userid", "");
		txt_email.setText(email);
		
		img_terms.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),Terms_Activity.class);
				startActivity(i);
			}
		});
		
		img_back=(ImageButton)findViewById(R.id.imgbtn_acct_backbtn);
		img_back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
	}

}
