package com.egrab.app;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

@SuppressLint("NewApi")
public class Categories_Activity extends BaseActivity {

	// declaration of variables
	TextView txt_noofcarts;

	private String[] navMenuTitles;
	private TypedArray navMenuIcons;
	private DrawerLayout mDrawerLayout;
	public boolean mSlideState = false;

	ImageButton imgbtn_menu, imgbtn_cart;
	Intent intent;
	ListView store_listview;
	String zoneid, storeid;

	ExpandableListAdapter adapter;
	SharedPreferences pref;
	Editor editor;
	private int lastExpandedPosition = -1;
	public ProgressDialog proDialog;

	public void startLoading() {
		proDialog = new ProgressDialog(this);
		proDialog.setMessage("Loading .Please Wait.....");
		proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		proDialog.setCancelable(false);
		proDialog.show();
	}

	public void stopLoading() {
		proDialog.dismiss();
		proDialog = null;
	}

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.activity_categories);
		getActionBar().hide();

		navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);
		navMenuIcons = getResources()
				.obtainTypedArray(R.array.nav_drawer_icons);

		set(navMenuTitles, navMenuIcons);

		mSlideState = false;

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

		mDrawerLayout.setDrawerListener(new ActionBarDrawerToggle(this,
				mDrawerLayout, R.drawable.ic_launcher, 0, 0) {

			@Override
			public void onDrawerClosed(View drawerView) {
				super.onDrawerClosed(drawerView);
				mSlideState = false;// is Closed
			}

			@Override
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				mSlideState = true;// is Opened
			}
		});

		ApplicationClass appState = ((ApplicationClass) getApplicationContext());

		pref = appState.pref;
		editor = appState.editor;

		txt_noofcarts = (TextView) findViewById(R.id.txt_categories_noofcart);

		imgbtn_cart = (ImageButton) findViewById(R.id.imgbtn_categories_cart);
		imgbtn_menu = (ImageButton) findViewById(R.id.imgbtn_categories_menulist);
		// store_listview=(ListView)findViewById(R.id.listview_storesdetails_storelist);
		// store_listview.setAdapter(catAdapter);
		final ArrayList<ExpandableListParentClass> categorymain = new ArrayList<ExpandableListParentClass>();
		final ExpandableListView myExpList = (ExpandableListView) findViewById(R.id.explistview_categories_categlist);

		// DisplayMetrics metrics = new DisplayMetrics();
		// getWindowManager().getDefaultDisplay().getMetrics(metrics);
		// int width = metrics.widthPixels;
		// myExpList.setIndicatorBounds(width - GetPixelFromDips(50), width -
		// GetPixelFromDips(10));

		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		int width = metrics.widthPixels;
		if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
			myExpList.setIndicatorBounds(width - GetPixelFromDips(50), width
					- GetPixelFromDips(10));
			myExpList.setIndicatorBounds(width - GetPixelFromDips(50), width
					- GetPixelFromDips(10));
		} else {
			myExpList.setIndicatorBoundsRelative(width - GetPixelFromDips(50),
					width - GetPixelFromDips(10));
			myExpList.setIndicatorBoundsRelative(width - GetPixelFromDips(50),
					width - GetPixelFromDips(10));
		}

		// ExpandableListAdapter adapter = (ExpandableListAdapter)
		// myExpList.getExpandableListAdapter();

		// zone api not got for customer
		// where from get customer id and zone id
		startLoading();
		zoneid = getIntent().getStringExtra("zone");
		storeid = getIntent().getStringExtra("storeid");

		Log.e("store id ====", storeid);

		String url = "http://egrab.qa/en/webservices/category.php";
		if (Utilities_Functions.checkinternet(getApplicationContext())) {
			Ion.with(getApplicationContext()).load(url).asJsonObject()
					.setCallback(new FutureCallback<JsonObject>() {
						@Override
						public void onCompleted(Exception e, JsonObject result) {
							stopLoading();
							if (e == null) {
								boolean status = result.get("success")
										.getAsBoolean();
								if (status) {
									JsonArray jsonarray = result.get("items")
											.getAsJsonArray();
									for (int i = 0; i < jsonarray.size(); i++) {
										JsonObject jsonobj = jsonarray.get(i)
												.getAsJsonObject();

										ExpandableListParentClass item = new ExpandableListParentClass();

										Category_Class catClass = new Category_Class();
										catClass.CategoryId = jsonobj.get(
												"CategoryID").getAsString();
										catClass.Categoryname = jsonobj.get(
												"CategoryName").getAsString();

										Log.e("catid======",
												catClass.CategoryId);
										Log.e("cat name===",
												catClass.Categoryname);

										item.setParent(catClass);
										categorymain.add(item);
									}
									adapter = new ExpandableListAdapter(
											getApplicationContext(),
											categorymain);
									myExpList.setAdapter(adapter);

									View footer_view = getLayoutInflater()
											.inflate(R.layout.footer, null);
									ImageView imv_advertisement_cat = (ImageView) footer_view
											.findViewById(R.id.imv_advertise_cat);
									Ion.with(imv_advertisement_cat)
											.placeholder(R.drawable.ic_launcher)
											.error(R.drawable.ic_launcher)
											.load(StoresDetails_Activity.ad_image_url);
									myExpList.addFooterView(footer_view/*
																		 * getLayoutInflater
																		 * (
																		 * ).inflate
																		 * (
																		 * R.layout
																		 * .
																		 * footer
																		 * ,
																		 * null)
																		 */);
									// Utilities_Functions.setListViewHeight(myExpList,
									// 0);

								} else {
									// error dialog here;
									Log.e(" not logged in", "sign up");
								}

							} else {
								Toast.makeText(
										getApplicationContext(),
										"Please Check your internet connection",
										Toast.LENGTH_LONG).show();
							}
						}
					});

			myExpList.setOnGroupExpandListener(new OnGroupExpandListener() {

				@Override
				public void onGroupExpand(int groupPosition) {
					if (lastExpandedPosition != -1
							&& groupPosition != lastExpandedPosition) {
						myExpList.collapseGroup(lastExpandedPosition);
					}
					lastExpandedPosition = groupPosition;

				}
			});

			myExpList.setOnGroupClickListener(new OnGroupClickListener() {

				@Override
				public boolean onGroupClick(ExpandableListView parent, View v,
						int groupPosition, long id) {

					startLoading();

					// TODO Auto-generated method stub
					Log.e("hihihihihi==================", "clicked");
					final int parentpos = groupPosition;

					String url = "http://egrab.qa/en/webservices/subcat.php?catid="
							+ adapter.getMParent().get(groupPosition)
									.getParent().CategoryId
							+ "&store="
							+ storeid;
					Ion.with(getApplicationContext()).load(url).asJsonObject()
							.setCallback(new FutureCallback<JsonObject>() {
								@Override
								public void onCompleted(Exception e,
										JsonObject result) {
									// Log.e("get results data",
									// result.toString());

									stopLoading();
									if (e == null) {
										boolean status = result.get("success")
												.getAsBoolean();
										if (status) {
											// for(int
											// i=0;i<adapter.getMParent().get(parentpos).getParentChildren().size();i++){
											// //
											// mParent.get(parentpos).getParentChildren().remove(i);
											// adapter.getMParent().get(parentpos).getParentChildren().remove(i);
											// }
											//
											adapter.getMParent().get(parentpos)
													.removeallchilds();
											adapter.notifyDataSetChanged();
											adapter.notifyDataSetInvalidated();

											JsonArray jsonarray = result.get(
													"items").getAsJsonArray();
											for (int i = 0; i < jsonarray
													.size(); i++) {
												JsonObject jsonobj = jsonarray
														.get(i)
														.getAsJsonObject();

												Category_Class subcategory = new Category_Class();
												subcategory.CategoryId = jsonobj
														.get("subcatid")
														.getAsString();
												subcategory.Categoryname = jsonobj
														.get("subcatname")
														.getAsString();

												// mParent.get(parentpos).getParentChildren().add(subcategory);
												adapter.getMParent()
														.get(parentpos)
														.getParentChildren()
														.add(subcategory);

												adapter.notifyDataSetChanged();
												adapter.notifyDataSetInvalidated();

												Log.e(" setcompleted",
														"=======");
											}
											// Utilities_Functions.setListViewHeight(myExpList,
											// parentpos);

										} else {
											// error dialog here;
											Log.e(" not logged in", "sign up");
										}
									} else {
										Toast.makeText(
												getApplicationContext(),
												"Please Check your internet connection",
												Toast.LENGTH_LONG).show();
									}
								}
							});

					return false;
				}
			});

			myExpList.setOnChildClickListener(new OnChildClickListener() {

				@Override
				public boolean onChildClick(ExpandableListView parent, View v,
						int groupPosition, int childPosition, long id) {
					// TODO Auto-generated method stub

					Intent i = new Intent(getApplicationContext(),
							ProductList_Activity.class);
					i.putExtra(
							"subcatid",
							adapter.getMParent().get(groupPosition)
									.getParentChildren().get(childPosition).CategoryId);

					i.putExtra(
							"subcatname",
							adapter.getMParent().get(groupPosition)
									.getParentChildren().get(childPosition).Categoryname);

					i.putExtra(
							"catname",
							adapter.getMParent().get(groupPosition).getParent().Categoryname);
					i.putExtra("catid", adapter.getMParent().get(groupPosition)
							.getParent().CategoryId);

					i.putExtra("storeid", storeid);

					startActivity(i);

					return false;
				}
			});
		}

		else {
			stopLoading();
			Toast.makeText(getApplicationContext(),
					"Please Check Your internet connection", Toast.LENGTH_LONG)
					.show();
		}
		imgbtn_cart.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent i = new Intent(getApplicationContext(),
						Cart_Activity.class);
				startActivity(i);
			}
		});

		// String zone="";
		//
		// viewStores(zone);
		//

	}

	// public void viewStores(String zone){
	//
	// Ion.with(getApplicationContext())
	// .load("http://egrab.qa/en/webservices/store.php?zone="+zone)
	// .asJsonObject()
	// .setCallback(new FutureCallback<JsonObject>() {
	// @Override
	// public void onCompleted(Exception e, JsonObject result) {
	// boolean status=result.get("success").getAsBoolean();
	// if(status){
	// JsonArray jsonarray=result.get("items").getAsJsonArray();
	// for (int i = 0; i < jsonarray.size(); i++) {
	// JsonObject jsonobj=jsonarray.get(i).getAsJsonObject();
	// // catAdapter.add(jsonobj);
	//
	// }
	//
	// }
	// else{
	// //error dialog here;
	// Log.e(" not logged in", "sign up");
	// }
	//
	//
	// }
	// });
	//
	// }

	@Override
	public void onResume() {
		super.onResume();
		// put your code here...
		if (pref.contains("noofcart")) {
			txt_noofcarts.setText("" + pref.getInt("noofcart", 0));
			txt_noofcarts.setVisibility(View.VISIBLE);
		} else {
			txt_noofcarts.setVisibility(View.GONE);
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);

	}

	public void showNavigationBar(View view) {

		if (mSlideState) {
			mDrawerLayout.closeDrawer(Gravity.LEFT);
		} else {
			mDrawerLayout.openDrawer(Gravity.LEFT);
		}
	}

	public int GetPixelFromDips(float pixels) {
		// Get the screen's density scale
		final float scale = getResources().getDisplayMetrics().density;
		// Convert the dps to pixels, based on density scale
		return (int) (pixels * scale + 0.5f);
	}

}