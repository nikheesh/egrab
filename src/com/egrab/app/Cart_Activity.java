package com.egrab.app;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

public class Cart_Activity extends Activity {
	// declaration of variables
	TextView txt_subtotal, txt_delivery_fee, txt_totalprice, txt_noitems;

	ImageButton imgbtn_back;
	Button btn_confirm;
	Intent intent;

	com.egrab.app.ExpandableHeightListView cart_listview, list_add_more;
	ArrayAdapter<JsonObject> cartAdapter;
	SharedPreferences pref;
	Editor editor;
	LinearLayout ll_cartitem_main, lnr_add_items;

	float f_subtotal, f_total, f_delivery;

	String custid;
	int noofcarts;
	DbHelper db_helper;
	public ProgressDialog proDialog;

	public void startLoading() {
		proDialog = new ProgressDialog(this);
		proDialog.setMessage("Loading. Please Wait.....");

		proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		proDialog.setCancelable(false);
		proDialog.show();
	}

	public void stopLoading() {
		proDialog.dismiss();
		proDialog = null;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_cart);

		ApplicationClass appState = ((ApplicationClass) getApplicationContext());

		pref = appState.pref;
		editor = appState.editor;
		db_helper = new DbHelper(Cart_Activity.this);

		cartAdapter = new ArrayAdapter<JsonObject>(this, 0) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {

				if (convertView == null)

					convertView = getLayoutInflater().inflate(
							R.layout.cart_item, null);

				final JsonObject json = getItem(position);

				// ll_cartitem_main =
				// (LinearLayout)findViewById(R.id.ll_cartitem_main);

				TextView txt_itemname = (TextView) convertView
						.findViewById(R.id.txt_cart_itemname);
				TextView txt_price = (TextView) convertView
						.findViewById(R.id.txt_cart_price);
				TextView txt_qty = (TextView) convertView
						.findViewById(R.id.txt_cart_item_qty);
				TextView txt_tprice = (TextView) convertView
						.findViewById(R.id.txt_cart_totalprice);

				ImageView img_prodimg = (ImageView) convertView
						.findViewById(R.id.img_cart_prodimg);
				ImageButton btn_delete = (ImageButton) convertView
						.findViewById(R.id.btn_cart_delete);
				txt_itemname.setText(json.get("description").getAsString());
				txt_price.setText(json.get("price").getAsString());
				txt_qty.setText(json.get("quantity").getAsString());
				txt_tprice.setText(json.get("subtotal").getAsString());

				String pimg = "http://www.egrab.qa/en/products/"
						+ json.get("resizeimg").getAsString();

				Ion.with(img_prodimg).load(pimg);

				btn_delete.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {

						// TODO Auto-generated method stub
						// http://egrab.qa/en/webservices/deletecart.php?prodid=2&customerid=34
						final String pid = json.get("prodid").getAsString();
						Ion.with(getApplicationContext())
								.load("http://egrab.qa/en/webservices/deletecart.php?prodid="
										+ pid + "&customerid=" + custid)
								.asJsonObject()
								.setCallback(new FutureCallback<JsonObject>() {
									@Override
									public void onCompleted(Exception e,
											JsonObject result) {
										if (e == null) {
											boolean status = result.get(
													"success").getAsBoolean();

											if (status) {

												final String calc_pid = json
														.get("prodid")
														.getAsString();

												String str_calc_price = json
														.get("price")
														.getAsString();
												str_calc_price = str_calc_price.substring(
														str_calc_price
																.indexOf(" "),
														str_calc_price.length());

												final float calc_price = Float
														.parseFloat(str_calc_price);
												final int calc_quan = json.get(
														"quantity").getAsInt();
												final float calc_minus_total = calc_quan
														* calc_price;

												f_subtotal = f_subtotal
														- calc_minus_total;
												f_total = f_subtotal;

												txt_subtotal.setText(Float
														.toString(f_subtotal));
												txt_totalprice.setText(Float
														.toString(f_subtotal));

												txt_subtotal.setText("QR "
														+ String.format("%.2f",
																f_subtotal));
												txt_totalprice.setText("QR "
														+ String.format("%.2f",
																f_subtotal));

												Log.d("vajid",
														"price : "
																+ Float.toString(calc_price));
												Log.d("vajid",
														"price : "
																+ Float.toString(calc_quan));
												Log.d("vajid",
														"price : "
																+ Float.toString(calc_minus_total));
												Log.d("vajid",
														"price : "
																+ Float.toString(f_subtotal));

												cartAdapter.remove(json);
												cartAdapter
														.notifyDataSetChanged();

												if (pref.contains("noofcart")) {
													noofcarts = pref.getInt(
															"noofcart", 0);
													if (noofcarts == 1) {
														editor.remove("noofcart");
														editor.commit();
													} else if (noofcarts > 1) {
														noofcarts--;
														editor.putInt(
																"noofcart",
																noofcarts);
														editor.commit();
													} else {
														editor.remove("noofcart");
														editor.commit();
													}

													if (pref.contains(pid)) {
														editor.remove(pid);
														editor.commit();
													}

												}

											}

											if (cart_listview.getCount() < 1) {
												btn_confirm.setEnabled(false);
											}

										} else {
											Toast.makeText(
													getApplicationContext(),
													"Please Check your internet connection",
													Toast.LENGTH_LONG).show();
										}
									}
								});
					}
				});

				return convertView;
			}

			@Override
			public int getViewTypeCount() {
				// TODO Auto-generated method stub
				return 2;
			}

			@Override
			public int getItemViewType(int position) {
				// TODO Auto-generated method stub
				return super.getItemViewType(position);
			}
		};

		txt_noitems = (TextView) findViewById(R.id.txt_cartacty_nocartitem);
		txt_subtotal = (TextView) findViewById(R.id.txt_cartacty_subtotalprice);
		txt_delivery_fee = (TextView) findViewById(R.id.txt_cartacty_deliveryprice);
		txt_totalprice = (TextView) findViewById(R.id.txt_cartacty_totalprice);

		imgbtn_back = (ImageButton) findViewById(R.id.imgbtn_cartacty_backbtn);
		btn_confirm = (Button) findViewById(R.id.btn_cartacty_confirm);

		cart_listview = (com.egrab.app.ExpandableHeightListView) findViewById(R.id.listview_cartacty_list);
		cart_listview.setAdapter(cartAdapter);
		cart_listview.setExpanded(true);

		list_add_more = (com.egrab.app.ExpandableHeightListView) findViewById(R.id.list_add_more);
		ViewGroup footer = (ViewGroup) getLayoutInflater().inflate(
				R.layout.extra_item_header, list_add_more, false);
		ArrayList<HashMap<String, String>> special_req_arr = db_helper
				.getAllProducts();
		if (special_req_arr.size() != 0) {
			list_add_more.addHeaderView(footer);

			Log.i("data from database", "" + special_req_arr);
			AddMoreAdapter add_adapter = new AddMoreAdapter(Cart_Activity.this,
					special_req_arr);
			list_add_more.setAdapter(add_adapter);
			list_add_more.setExpanded(true);
			add_adapter.notifyDataSetChanged();
		}

		// ApplicationClass appState =
		// ((ApplicationClass)getApplicationContext());

		pref = appState.pref;
		custid = pref.getString("customerid", "0");

		// zone api not got for customer
		// where from get customer id and zone id

		if (Utilities_Functions.checkinternet(getApplicationContext())) {

			btn_confirm.setEnabled(false);
			Ion.with(getApplicationContext())
					.load("http://egrab.qa/en/webservices/viewcart.php?customerid="
							+ custid).asJsonObject()
					.setCallback(new FutureCallback<JsonObject>() {
						@Override
						public void onCompleted(Exception e, JsonObject result) {
							if (e == null) {
								boolean status = result.get("success")
										.getAsBoolean();
								if (status) {
									JsonArray jsonarray = result.get("items")
											.getAsJsonArray();
									Log.e("cart items received==============================",
											jsonarray.toString());
									for (int i = 0; i < jsonarray.size(); i++) {
										JsonObject jsonobj = jsonarray.get(i)
												.getAsJsonObject();
										cartAdapter.add(jsonobj);
									}

									txt_totalprice.setText(result.get(
											"producttotal").getAsString());
									txt_subtotal.setText(result.get(
											"producttotal").getAsString());
									txt_delivery_fee.setText("0.00");

									String f_string_subtotal = result.get(
											"producttotal").getAsString();

									f_string_subtotal = f_string_subtotal
											.substring(f_string_subtotal
													.indexOf(" "),
													f_string_subtotal.length());
									f_subtotal = Float
											.parseFloat(f_string_subtotal);
									f_total = f_subtotal;

									Log.d("vajid", "api response subtotal "
											+ f_string_subtotal);

									noofcarts = result.get("carttitem")
											.getAsInt();

									if (noofcarts > 0) {
										btn_confirm.setEnabled(true);
										txt_noitems.setVisibility(View.GONE);
										cart_listview
												.setVisibility(View.VISIBLE);
									} else {
										txt_noitems.setVisibility(View.VISIBLE);
										cart_listview.setVisibility(View.GONE);
										btn_confirm.setEnabled(false);
									}
									editor.putInt("noofcart", noofcarts);
									editor.commit();

								} else {
									// error dialog here;
									txt_noitems.setVisibility(View.VISIBLE);
									cart_listview.setVisibility(View.GONE);
									btn_confirm.setEnabled(false);
									Log.e(" not logged in", "sign up");
								}

							}

							else {
								Toast.makeText(
										getApplicationContext(),
										"Please Check your internet connection",
										Toast.LENGTH_LONG).show();
							}

						}
					});

		} else {
			Toast.makeText(getApplicationContext(), "No internet connection",
					Toast.LENGTH_LONG).show();
		}

		imgbtn_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		String storeid;

		storeid = pref.getString("storeid", "");

		if (!storeid.equals("11")) {

			btn_confirm.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// http://egrab.qa/en/webservices/confirmorder.php?customerid=34
					ArrayList<String> pids = new ArrayList<String>();
					for (int i = 0; i < cartAdapter.getCount(); i++) {
						String pid = cartAdapter.getItem(i).get("prodid")
								.getAsString();
						pids.add(pid);
					}

					Intent i = new Intent(getApplicationContext(),
							Confirm_Activity.class);
					i.putExtra("totalprice", txt_totalprice.getText()
							.toString());
					i.putExtra("pids", pids);
					startActivity(i);

				}
			});
		} else {
			btn_confirm.setEnabled(false);
			Toast.makeText(Cart_Activity.this,
					"Not available on the selected Location", 1000).show();
		}

	}

	public void checkClick(View view) {

		// Toast.makeText(getApplicationContext(), "The click got called",
		// Toast.LENGTH_SHORT).show();

	}

	public void doClickDelButton(View view) {

		ImageButton imgbtn = (ImageButton) findViewById(R.id.btn_cart_delete);
		imgbtn.performClick();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}