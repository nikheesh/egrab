package com.egrab.app;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.AudioManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.JsonObject;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

public class Confirm_Activity extends FragmentActivity implements
		OnCameraChangeListener {

	TextView txt_total;
	Button btn_getcurrentlocation, btn_placeorder;
	ImageButton imgback;
	EditText edit_email, edit_phone, edit_buildno, edit_zone, edit_street,
			edit_addr, edit_notes;
	ToggleButton tgl_saveaddr;
	LatLng latlng, currentlatlng;
	static ArrayList<String> listofpid;

	SupportMapFragment map;

	SharedPreferences pref;
	Editor editor;
	public static String buildno;
	public static String zone;
	public static String streetno;
	public static String addr;
	public static String phone;
	public static String email;
	public static String notes;
	public static String custid;
	public static String lat;
	public static String lng;

	public ProgressDialog proDialog;

	public void startLoading() {
		proDialog = new ProgressDialog(this);
		proDialog.setMessage("Please Wait.....");
		proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		proDialog.setCancelable(false);
		proDialog.show();
	}

	public void stopLoading() {
		proDialog.dismiss();
		proDialog = null;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);

		// getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,

		// WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_confirm);

		txt_total = (TextView) findViewById(R.id.txt_confirm_totalprice);

		btn_getcurrentlocation = (Button) findViewById(R.id.btn_confirm_getclocation);
		btn_placeorder = (Button) findViewById(R.id.btn_confirm_placeorder);
		imgback = (ImageButton) findViewById(R.id.imgbtn_confirm_backbtn);
		edit_addr = (EditText) findViewById(R.id.edit_confirm_address);
		edit_buildno = (EditText) findViewById(R.id.edit_confirm_buildno);
		edit_email = (EditText) findViewById(R.id.edit_confirm_email);
		edit_notes = (EditText) findViewById(R.id.edit_confirm_notes);
		edit_phone = (EditText) findViewById(R.id.edit_confirm_phone);
		String code = "+974";
		edit_phone.setCompoundDrawablesWithIntrinsicBounds(new TextDrawable(
				code), null, null, null);
		edit_phone.setCompoundDrawablePadding(code.length() * 15);
		edit_street = (EditText) findViewById(R.id.edit_confirm_streetno);
		edit_zone = (EditText) findViewById(R.id.edit_confirm_zone);

		tgl_saveaddr = (ToggleButton) findViewById(R.id.toggle_confirm_saveaddr);
		ApplicationClass appState = ((ApplicationClass) getApplicationContext());

		pref = appState.pref;
		editor = appState.editor;

		imgback.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		String tprice = getIntent().getStringExtra("totalprice");
		listofpid = getIntent().getStringArrayListExtra("pids");

		final LocationManager locationManager = (LocationManager) this
				.getSystemService(Context.LOCATION_SERVICE);

		// Define a listener that responds to location updates
		final LocationListener locationListener = new LocationListener() {
			@Override
			public void onLocationChanged(Location location) {
				// Called when a new location is found by the network location
				// provider.
				currentlatlng = new LatLng(location.getLatitude(),
						location.getLongitude());

			}

			@Override
			public void onStatusChanged(String provider, int status,
					Bundle extras) {
			}

			@Override
			public void onProviderEnabled(String provider) {
			}

			@Override
			public void onProviderDisabled(String provider) {
			}
		};

		if (locationManager.getAllProviders().contains(
				LocationManager.NETWORK_PROVIDER))
			locationManager.requestLocationUpdates(
					LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
		if (locationManager.getAllProviders().contains(
				LocationManager.GPS_PROVIDER))
			locationManager.requestLocationUpdates(
					LocationManager.GPS_PROVIDER, 0, 0, locationListener);

		if (currentlatlng != null) {
			latlng = currentlatlng;
		}
		map = (SupportMapFragment) this.getSupportFragmentManager()
				.findFragmentById(R.id.gmap_confrirm_map);
		map.getMap().setMyLocationEnabled(true);

		map.getMap().getUiSettings().setZoomControlsEnabled(true);

		map.getMap().setOnMapLongClickListener(
				new GoogleMap.OnMapLongClickListener() {

					@Override
					public void onMapLongClick(LatLng point) {
						// TODO Auto-generated method stub
						latlng = point;
						map.getMap().clear();
						Marker marker = map.getMap().addMarker(
								new MarkerOptions().position(point));
						marker.setIcon(BitmapDescriptorFactory
								.fromResource(R.drawable.mapicon));
						// Log.e("new lat lng", latlng.toString());
					}
				});

		btn_getcurrentlocation.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				boolean gps_enabled = false;
				boolean network_enabled = false;

				try {
					gps_enabled = locationManager
							.isProviderEnabled(LocationManager.GPS_PROVIDER);
				} catch (Exception ex) {
				}

				try {
					network_enabled = locationManager
							.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
				} catch (Exception ex) {
				}

				if (!gps_enabled && !network_enabled) {
					// notify user
					AlertDialog.Builder dialog = new AlertDialog.Builder(
							Confirm_Activity.this);
					dialog.setMessage("Check your Location Settings first");
					dialog.setPositiveButton("ok",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(
										DialogInterface paramDialogInterface,
										int paramInt) {
									// TODO Auto-generated method stub
									Intent myIntent = new Intent(
											Settings.ACTION_LOCATION_SOURCE_SETTINGS);
									Confirm_Activity.this
											.startActivity(myIntent);
									// get gps
								}
							});
					dialog.setNegativeButton("Cancel",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(
										DialogInterface paramDialogInterface,
										int paramInt) {
									// TODO Auto-generated method stub

								}
							});
					dialog.show();
				}

				if (currentlatlng != null) {
					latlng = currentlatlng;
					map.getMap().clear();
					CameraUpdate cameraPosition = CameraUpdateFactory
							.newLatLngZoom(latlng, 16);

					map.getMap().animateCamera(cameraPosition);

					Marker marker = map.getMap().addMarker(
							new MarkerOptions().position(latlng));
					marker.setIcon(BitmapDescriptorFactory
							.fromResource(R.drawable.mapicon));
					// Log.e("new lat lng", latlng.toString());

				} else if (latlng != null) {
					map.getMap().clear();
					CameraUpdate cameraPosition = CameraUpdateFactory
							.newLatLngZoom(latlng, 16);
					map.getMap().animateCamera(cameraPosition);
					Marker marker = map.getMap().addMarker(
							new MarkerOptions().position(latlng));
					marker.setIcon(BitmapDescriptorFactory
							.fromResource(R.drawable.mapicon));
				}

			}
		});

		txt_total.setText(tprice);
		if (pref.contains("userid")) {
			edit_email.setText(pref.getString("userid", "0"));
		}
		if (pref.contains("buildno")) {
			edit_buildno.setText(pref.getString("buildno", "0"));
		}
		if (pref.contains("zone")) {
			edit_zone.setText(pref.getString("zone", "0"));
		}
		if (pref.contains("streetno")) {
			edit_street.setText(pref.getString("streetno", "0"));
		}
		if (pref.contains("address")) {
			edit_addr.setText(pref.getString("address", "0"));
		}

		btn_placeorder.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (latlng != null) {

					buildno = edit_buildno.getText().toString();
					zone = edit_zone.getText().toString();
					streetno = edit_street.getText().toString();
					addr = edit_addr.getText().toString();
					phone = edit_phone.getText().toString();
					email = edit_email.getText().toString();
					notes = edit_notes.getText().toString();
					custid = pref.getString("customerid", "0");
					 lat = "" + latlng.latitude;
					lng = "" + latlng.longitude;
					String errString = "";
					boolean errvalue = false;

					PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
					PhoneNumber phonenum = null;
					try {
						phonenum = phoneUtil.parse("+974" + phone, "QA");
						Log.e("phone number==", phonenum.toString());
					} catch (NumberParseException e) {
						// System.err.println("NumberParseException was thrown: "
						// + e.toString());
					}

					if (phone.length() == 0 || phone.equals("")) {
						errString = "Please enter your phone number";
						errvalue = true;
					} else if (!phoneUtil.isValidNumber(phonenum)) {
						errString = "Please enter valid contact number";
						errvalue = true;
					}
					if (addr.length() == 0) {
						errString = "enter address";
						errvalue = true;
					}
					if (!errvalue) {

						// startLoading();

						if (tgl_saveaddr.isChecked()) {

							editor.putString("buildno", buildno);
							editor.putString("zone", zone);
							editor.putString("streetno", streetno);
							editor.putString("address", addr);
							editor.commit();

						}
						startActivity(new Intent(Confirm_Activity.this,
								DeliverySlot.class));

						// final String
						// url="http://egrab.qa/en/webservices/order.php?customerid="+custid+"&customer_address="+addr+"&customer_zone="+zone+"&mobile_number="+phone+"&streat="+streetno+"&building_number="+buildno+"&order_notes="+notes+"&lat="+latlng.latitude+"&lng="+latlng.longitude;

						// Commenting from here
						/*
						 * final String url =
						 * "http://egrab.qa/en/webservices/order.php";
						 * 
						 * Ion.with(getApplicationContext()).load(url)
						 * .setBodyParameter("customerid", custid)
						 * .setBodyParameter("customer_address", addr)
						 * .setBodyParameter("customer_zone", zone)
						 * .setBodyParameter("mobile_number", phone)
						 * .setBodyParameter("streat", streetno)
						 * .setBodyParameter("building_number", buildno)
						 * .setBodyParameter("order_notes", notes)
						 * .setBodyParameter("lat", "" + latlng.latitude)
						 * .setBodyParameter("lng", "" + latlng.longitude)
						 * .asJsonObject() .setCallback(new
						 * FutureCallback<JsonObject>() {
						 * 
						 * @Override public void onCompleted(Exception e,
						 * JsonObject result) { if (e == null) { boolean status
						 * = result.get( "success").getAsBoolean(); if (status)
						 * { JsonObject json = result.get( "items")
						 * .getAsJsonObject(); String orderid = Integer
						 * .toString(json.get( "orderid") .getAsInt());
						 * stopLoading(); if (listofpid != null) { for (int i =
						 * 0; i < listofpid .size(); i++) {
						 * editor.remove(listofpid .get(i)); editor.commit();
						 * Log.e(listofpid.get(i), "removed"); } }
						 * 
						 * editor.remove("noofcart"); editor.commit();
						 * 
						 * Intent i = new Intent( getApplicationContext(),
						 * CompletedOrder_Activity.class); i.putExtra("orderid",
						 * orderid); startActivity(i);
						 * 
						 * } else { stopLoading(); Toast.makeText(
						 * getApplicationContext(),
						 * "your order can not be confirm now",
						 * Toast.LENGTH_LONG) .show(); } } else { stopLoading();
						 * Log.e("internet   url  ", url + ""); Toast.makeText(
						 * getApplicationContext(),
						 * "Please Check your internet connection",
						 * Toast.LENGTH_LONG).show(); }
						 * 
						 * }
						 * 
						 * });
						 */
						// comment ends here

					} else {
						Toast.makeText(getApplicationContext(), errString,
								Toast.LENGTH_LONG).show();
					}

				} else {
					Toast.makeText(
							getApplicationContext(),
							"cant got your location,check your gps or network settings",
							Toast.LENGTH_LONG).show();
				}

			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// getMenuInflater().inflate(R.menu.near_by_map, menu);
		return true;
	}

	@Override
	public void onCameraChange(CameraPosition arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		// if(widIntent!=null){
		// stopService(widIntent);
		// }
	}

}
