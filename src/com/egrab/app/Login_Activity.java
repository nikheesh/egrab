package com.egrab.app;


import java.io.File;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.Future;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

public class Login_Activity  extends Activity {
	//declaration of variables
	TextView forgotpass_txtview,createnew_txtview;
	EditText email_edit,password_edit;
	Button signin_btn;
	Intent intent;
	String email,pass;
	VideoView video1;
	
	private boolean bVideoIsBeingTouched = false;
	private Handler mHandler = new Handler();
	private int media_position = 1;
	public  ProgressDialog proDialog;
	public  void startLoading() {
	    proDialog = new ProgressDialog(this);
	    proDialog.setMessage("Signing in. Please Wait.....");
	    proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	    proDialog.setCancelable(false);
	    proDialog.show();
	}

public  void stopLoading() {
	    proDialog.dismiss();
	    proDialog = null;
	}
	
	 Future<File> loadfile;
	 Future<JsonObject> loadingfile;
	
	 boolean validationError;
	 String errorMessage;
	
	 @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		setContentView(R.layout.activity_login);
		
		email_edit=(EditText)findViewById(R.id.edit_signin_email);
		
		password_edit=(EditText)findViewById(R.id.edit_signin_password);
		signin_btn=(Button)findViewById(R.id.btn_signin_signin);
		createnew_txtview=(TextView)findViewById(R.id.txt_signin_signup);
		forgotpass_txtview=(TextView)findViewById(R.id.txt_signin_forgot);
	
		password_edit.setTypeface(Typeface.DEFAULT);
		password_edit.setTransformationMethod(new PasswordTransformationMethod());
		
		signin_btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				email=email_edit.getText().toString();
				pass=password_edit.getText().toString();
				if(email.length()!=0&&pass.length()!=0){
					if(Utilities_Functions.chekEmailId(email)){
					
				if(Utilities_Functions.checkinternet(getApplicationContext())){
					startLoading();
				Ion.with(getApplicationContext())
				.load("http://egrab.qa/en/webservices/login.php?email="+email+"&password="+pass)
				.setLogging("result", 1)
				.asJsonObject()
				
				.setCallback(new FutureCallback<JsonObject>() {
				   @Override
				    public void onCompleted(Exception e, JsonObject result) {
					   stopLoading();
					   if(e==null){
						   Log.e("result", result.toString());
				       boolean login_status=result.get("success").getAsBoolean();
				       if(login_status){
				    	   Log.e("logged in successfully", "ghd");
				    	   ApplicationClass appState = ((ApplicationClass)getApplicationContext());
				    	   Editor editor=appState.editor;
				    	   editor.putBoolean("islogedin",true);
				    	   editor.putString("userid",email);
				    	   editor.putString("password", pass);
				    	   editor.putString("customerid", result.get("customerid").getAsString());
				    	   editor.putString("zone", result.get("zone").getAsString());
				    	   editor.commit();
				    	   
				    	   finish();
				    	   intent=new Intent(getApplicationContext(),StoresDetails_Activity.class);
				    	 //  intent.putExtra("customerid", result.get("customerid").getAsString());
						   intent.putExtra("zone", result.get("zone").getAsString());
						   intent.putExtra("mode", "firstlogin");
						   startActivity(intent);
						  
				    	   
				       }
				       else{
				    	  Toast.makeText(getApplicationContext(), "login failed due to invalid credentials", Toast.LENGTH_LONG).show();
				       }
					   }
					   else{
							Toast.makeText(getApplicationContext(), "Please Check your internet connection", Toast.LENGTH_LONG).show();
						}
				    }
				});
				}
				else{
					Toast.makeText(getApplicationContext(), "Please check your internet connection", Toast.LENGTH_LONG).show();
				}
				
				
					}
					else {
						Toast.makeText(getApplicationContext(), "Enter a valid email id", Toast.LENGTH_LONG).show();
						
					}
		}
			else{
				Toast.makeText(getApplicationContext(), "Please add all fields", Toast.LENGTH_LONG).show();
			}
			}
		});
		
		
		forgotpass_txtview.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				email=email_edit.getText().toString();
				if(email.length()!=0){
					if(Utilities_Functions.chekEmailId(email)){
					
				if(Utilities_Functions.checkinternet(getApplicationContext())){
					startLoading();
				Ion.with(getApplicationContext())
				.load("http://egrab.qa/en/webservices/forgot.php?email="+email)
				.setLogging("result", 1)
				.asJsonObject()
				
				.setCallback(new FutureCallback<JsonObject>() {
				   @Override
				    public void onCompleted(Exception e, JsonObject result) {
					   stopLoading();
					   if(e==null){
						   Log.e("result", result.toString());
				       boolean login_status=result.get("success").getAsBoolean();
				       if(login_status){
				    	  
				    	   Toast.makeText(getApplicationContext(), "A Password reset mail has been send to your registered email id", Toast.LENGTH_LONG).show();
						    
				       }
				       else{
				    	  Toast.makeText(getApplicationContext(), "Invalid email id", Toast.LENGTH_LONG).show();
				       }
					   }
					   else{
							Toast.makeText(getApplicationContext(), "Please Check your internet connection", Toast.LENGTH_LONG).show();
						}
				    }
				});
				}
				else{
					Toast.makeText(getApplicationContext(), "Please check your internet connection", Toast.LENGTH_LONG).show();
				}
				
				
					}
					else {
						Toast.makeText(getApplicationContext(), "Enter a valid email id", Toast.LENGTH_LONG).show();
						
					}
		}
			else{
				Toast.makeText(getApplicationContext(), "Please add email field", Toast.LENGTH_LONG).show();
			}
			}
		});
		
		//create new account
		createnew_txtview.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {

				intent=new Intent(getApplicationContext(),CreateNew_Activity.class);
				startActivity(intent);
			}
		});
		
		
		
		video1=(VideoView)findViewById(R.id.video_signin_intro);
	    video1.setVideoURI(Uri.parse("android.resource://" +getPackageName()+ "/"+R.raw.egrabintro));
	    video1.setMediaController(null);
	    video1.requestFocus();
	    
	    video1.start();
	    
	    
	    video1.setOnTouchListener(new View.OnTouchListener() {
	        @Override
	        public boolean onTouch(View v, MotionEvent event) {
		        if (!bVideoIsBeingTouched) {
				        
		        	 bVideoIsBeingTouched = true;
		        	 
				     if (video1.isPlaying()) {
				    	    media_position = video1.getCurrentPosition();
				            video1.pause();
				     } else {

				    	    //video1.seekTo(media_position);
				            video1.start();				
				            
				     }
				     
				     mHandler.postDelayed(new Runnable() {
				            public void run() {
				                bVideoIsBeingTouched = false;
				            }
				      }, 100);
		        }
	        
		        return true;
	        
	        }
	    });   
		
		
	}
	
	 @Override
		public void onResume(){
		    super.onResume();
		    // put your code here...
		    video1.requestFocus();
		    
		    video1.start();
		    
		    
		}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
