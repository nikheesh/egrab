package com.egrab.app;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

public class StoresDetails_Activity extends BaseActivity {
	// declaration of variables
	TextView txt_location, txt_noofcarts, txt_loc_change, txt_noitems;

	private String[] navMenuTitles;
	private TypedArray navMenuIcons;
	private DrawerLayout mDrawerLayout;
	public boolean mSlideState = false;
	public static String ad_image_url;
	ImageButton imgbtn_menu, imgbtn_cart;
	Intent intent;
	ImageView imv_advertisement;
	ListView store_listview;
	ArrayAdapter<JsonObject> storeAdapter;
	// ArrayList<Map<String,String>> zones;
	SharedPreferences pref;
	Editor editor;
	String zoneid, custid;
	public ProgressDialog proDialog;

	public void startLoading(String msg) {
		proDialog = new ProgressDialog(this);
		proDialog.setMessage(msg);
		proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		proDialog.setCancelable(false);
		proDialog.show();
	}

	public void stopLoading() {
		proDialog.dismiss();
		proDialog = null;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		try {
			// this.requestWindowFeature(Window.FEATURE_NO_TITLE);
			setContentView(R.layout.activity_storesdetails);

			getActionBar().hide();

			navMenuTitles = getResources().getStringArray(
					R.array.nav_drawer_items);
			navMenuIcons = getResources().obtainTypedArray(
					R.array.nav_drawer_icons);

			set(navMenuTitles, navMenuIcons);

			mSlideState = false;

			mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

			mDrawerLayout.setDrawerListener(new ActionBarDrawerToggle(this,
					mDrawerLayout, R.drawable.ic_launcher, 0, 0) {

				@Override
				public void onDrawerClosed(View drawerView) {
					super.onDrawerClosed(drawerView);
					mSlideState = false;// is Closed
				}

				@Override
				public void onDrawerOpened(View drawerView) {
					super.onDrawerOpened(drawerView);
					mSlideState = true;// is Opened
				}
			});

			txt_noofcarts = (TextView) findViewById(R.id.txt_storesdetails_noofcart);
			txt_noitems = (TextView) findViewById(R.id.txt_storesdetails_nostoresitem);

			txt_location = (TextView) findViewById(R.id.txt_storesdetail_location);
			txt_loc_change = (TextView) findViewById(R.id.txt_storesdetail_changeloc);
			imv_advertisement = (ImageView) findViewById(R.id.imv_advertisement);
			// zones=new ArrayList<Map<String,String>>();
			storeAdapter = new ArrayAdapter<JsonObject>(this, 0) {
				@Override
				public View getView(int position, View convertView,
						ViewGroup parent) {
					if (convertView == null)
						convertView = getLayoutInflater().inflate(
								R.layout.storesdetails_item, null);

					final JsonObject json = getItem(position);

					ImageView imv_store_logo = (ImageView) convertView
							.findViewById(R.id.imv_store_logo);
					Ion.with(imv_store_logo)
							.placeholder(R.drawable.ic_launcher)
							.error(R.drawable.ic_launcher)
							.load(json.get("logo").getAsString());
					TextView tv_store_text1 = (TextView) convertView
							.findViewById(R.id.tv_store_text1);
					TextView tv_store_text2 = (TextView) convertView
							.findViewById(R.id.tv_store_text2);
					String mov_text, fee_text;
					if (json.get("mov").getAsString()
							.compareToIgnoreCase("0.00") == 0) {
						mov_text = "";
						mov_text = "No delivery fees:";
					} else {
						mov_text = "";
						mov_text = "Delivery fee:";
					}
					if (json.get("fees").getAsString()
							.compareToIgnoreCase("0.00") == 0) {
						fee_text = "";
						fee_text = "No minimum order";
					} else {
						fee_text = "";
						fee_text = "Minimum order value:";
					}
					tv_store_text1.setText(mov_text+json.get("mov").getAsString());
					tv_store_text2.setText(fee_text+json.get("fees").getAsString());
					TextView txt_storename = (TextView) convertView
							.findViewById(R.id.txt_storesdetails_item_storesname);
					txt_storename.setText(json.get("storename").getAsString());
					txt_storename.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							String storeid = json.get("storeid").getAsString();
							Intent i = new Intent(getApplicationContext(),
									Categories_Activity.class);
							i.putExtra("zone", zoneid);
							i.putExtra("storeid", storeid);

							startActivity(i);

						}
					});

					return convertView;
				}
			};

			ApplicationClass appState = ((ApplicationClass) getApplicationContext());

			pref = appState.pref;
			editor = appState.editor;
			String email = pref.getString("userid", "0");
			String pass = pref.getString("password", "0");
			String urlzone = "http://egrab.qa/en/webservices/login.php?email="
					+ email + "&password=" + pass;

			if (Utilities_Functions.checkinternet(getApplicationContext())) {
				startLoading("Loading. Please wait...");

				Ion.with(getApplicationContext()).load(urlzone).asJsonObject()
						.setCallback(new FutureCallback<JsonObject>() {
							@Override
							public void onCompleted(Exception e,
									JsonObject result) {
								if (proDialog != null)
									stopLoading();

								if (e == null) {

									boolean status = result.get("success")
											.getAsBoolean();
									if (status) {
										editor.putString("zone",
												result.get("zone")
														.getAsString());
										editor.commit();
										if (!pref.contains("customerid")) {
											editor.putString("customerid",
													result.get("customerid")
															.getAsString());
											editor.commit();
										}
										custid = pref.getString("customerid",
												"0");
										// getIntent().getStringExtra("customerid");
										zoneid = pref.getString("zone", "0");

										imgbtn_cart = (ImageButton) findViewById(R.id.imgbtn_storesdetails_cart);
										imgbtn_menu = (ImageButton) findViewById(R.id.imgbtn_storesdetails_menulist);
										store_listview = (ListView) findViewById(R.id.listview_storesdetails_storelist);
										store_listview.setAdapter(storeAdapter);

										// zone api not got for customer
										// where from get customer id and zone
										// id

										// getIntent().getStringExtra("zone");
										if (pref.contains("zonename")) {
											if (proDialog != null)
												stopLoading();
											txt_location.setText(pref
													.getString("zonename",
															"zone"));
										} else {
											if (Utilities_Functions
													.checkinternet(getApplicationContext())) {
												startLoading("Loading. Please wait...");

												Ion.with(
														getApplicationContext())
														.load("http://egrab.qa/en/webservices/zone.php")
														.asJsonObject()
														.setCallback(
																new FutureCallback<JsonObject>() {
																	@Override
																	public void onCompleted(
																			Exception e,
																			JsonObject result) {
																		if (proDialog != null)
																			stopLoading();
																		JsonArray jsonarray;
																		if (e == null) {
																			jsonarray = result
																					.get("items")
																					.getAsJsonArray();
																			for (int i = 0; i < jsonarray
																					.size(); i++) {
																				String zoneids = jsonarray
																						.get(i)
																						.getAsJsonObject()
																						.get("zoneID")
																						.getAsString();
																				String zonename = jsonarray
																						.get(i)
																						.getAsJsonObject()
																						.get("zoneName")
																						.getAsString();
																				if (zoneids
																						.equals(zoneid)) {
																					txt_location
																							.setText(zonename);
																					editor.putString(
																							"zonename",
																							zonename);
																					editor.commit();
																					break;
																				}

																			}
																		} else {
																			if (proDialog != null)
																				stopLoading();

																			Toast.makeText(
																					getApplicationContext(),
																					"Please Check your internet connection",
																					Toast.LENGTH_LONG)
																					.show();
																		}
																		if (proDialog != null)
																			stopLoading();

																	}
																});
											} else {
												Toast.makeText(
														getApplicationContext(),
														"Please check your internet connection",
														Toast.LENGTH_LONG)
														.show();
											}
										}

										if (proDialog == null)
											startLoading("Loading stores.please wait..");
										if (Utilities_Functions
												.checkinternet(getApplicationContext())) {
											String urlzone = "http://egrab.qa/en/webservices/store.php?zone="
													+ zoneid;
											Ion.with(getApplicationContext())
													.load(urlzone)
													.asJsonObject()
													.setCallback(
															new FutureCallback<JsonObject>() {
																@Override
																public void onCompleted(
																		Exception e,
																		JsonObject result) {
																	if (proDialog != null)
																		stopLoading();
																	if (e == null) {
																		Log.e("The eGrab Stores",
																				result.toString());
																		ad_image_url = result
																				.get("ad_image")
																				.getAsString();
																		Ion.with(
																				imv_advertisement)
																				.placeholder(
																						R.drawable.ic_launcher)
																				.error(R.drawable.ic_launcher)
																				.load(ad_image_url);

																		boolean status = result
																				.get("success")
																				.getAsBoolean();
																		if (status) {

																			JsonArray jsonarray = result
																					.get("items")
																					.getAsJsonArray();
																			if (jsonarray
																					.size() > 0) {
																				txt_noitems
																						.setVisibility(View.GONE);
																				for (int i = 0; i < jsonarray
																						.size(); i++) {

																					JsonObject jsonobj = jsonarray
																							.get(i)
																							.getAsJsonObject();
																					storeAdapter
																							.add(jsonobj);

																				}
																			} else {
																				store_listview
																						.setVisibility(View.GONE);
																				txt_noitems
																						.setVisibility(View.VISIBLE);
																			}

																		} else {

																			if (getIntent()
																					.getStringExtra(
																							"mode")
																					.equals("firstlogin")) {

																				editor.putString(
																						"storeid",
																						"11");
																				editor.commit();
																				Intent intent = new Intent(
																						getApplicationContext(),
																						Alert_Dialog_Activity.class);
																				startActivity(intent);
																				finish();

																			} else {
																				store_listview
																						.setVisibility(View.GONE);
																				txt_noitems
																						.setVisibility(View.VISIBLE);

																			}
																		}

																	} else {
																		Toast.makeText(
																				getApplicationContext(),
																				"Please Check your internet connection",
																				Toast.LENGTH_LONG)
																				.show();
																	}
																}
															});
										} else {
											if (proDialog != null)
												stopLoading();
											Toast.makeText(
													getApplicationContext(),
													"check your internet connection",
													Toast.LENGTH_LONG).show();
										}

										imgbtn_cart
												.setOnClickListener(new OnClickListener() {

													@Override
													public void onClick(View v) {
														// TODO Auto-generated
														// method stub

														Intent i = new Intent(
																getApplicationContext(),
																Cart_Activity.class);
														startActivity(i);
													}
												});

										txt_location
												.setOnClickListener(new OnClickListener() {

													@Override
													public void onClick(View v) {
														if (Utilities_Functions
																.checkinternet(getApplicationContext())) {
															Intent i = new Intent(
																	getApplicationContext(),
																	ZoneList_Activity.class);
															i.putExtra(
																	"customerid",
																	custid);
															editor.remove("zonename");
															editor.commit();
															startActivity(i);
														} else {
															Toast.makeText(
																	getApplicationContext(),
																	"Please Check your internet connection",
																	Toast.LENGTH_LONG)
																	.show();
														}
														// finish();

													}
												});

										txt_loc_change
												.setOnClickListener(new OnClickListener() {

													@Override
													public void onClick(View v) {
														if (Utilities_Functions
																.checkinternet(getApplicationContext())) {
															Intent i = new Intent(
																	getApplicationContext(),
																	ZoneList_Activity.class);
															i.putExtra(
																	"customerid",
																	custid);
															editor.remove("zonename");
															editor.commit();
															startActivity(i);
														} else {
															Toast.makeText(
																	getApplicationContext(),
																	"Please Check your internet connection",
																	Toast.LENGTH_LONG)
																	.show();
														}
														// finish();

													}
												});

									}
								} else {
									Toast.makeText(
											getApplicationContext(),
											"Please Check your internet connection",
											Toast.LENGTH_LONG).show();
								}

							}
						});
			} else {
				if (proDialog != null)
					stopLoading();

				Toast.makeText(getApplicationContext(), "Check Your Internet",
						Toast.LENGTH_LONG).show();
			}
		} catch (Exception e) {

		}

	}

	@Override
	public void onResume() {
		super.onResume();
		// put your code here...
		if (pref.contains("zonename")) {
			// if(proDialog!=null)
			// stopLoading();
			txt_location.setText(pref.getString("zonename", "zone"));
		}
		if (pref.contains("noofcart")) {
			txt_noofcarts.setText("" + pref.getInt("noofcart", 0));
			txt_noofcarts.setVisibility(View.VISIBLE);
		} else {
			txt_noofcarts.setVisibility(View.GONE);
		}

	}

	@Override
	public void onBackPressed() {
		finish();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	public void showNavigationBar(View view) {

		if (mSlideState) {
			mDrawerLayout.closeDrawer(Gravity.LEFT);
		} else {
			mDrawerLayout.openDrawer(Gravity.LEFT);
		}

	}

}