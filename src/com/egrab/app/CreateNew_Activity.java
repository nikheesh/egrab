package com.egrab.app;


import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Toast;


import com.google.gson.JsonArray;
import com.google.gson.JsonObject;



import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

public class CreateNew_Activity  extends Activity implements OnItemClickListener{
	
	//declaration of variables
	TextView signin_txtview;
	EditText name_edit,email_edit,password_edit;
	Button signup_btn;
	Intent intent;
	String name,loc,loc_name,email,pass;
	public  ProgressDialog proDialog;
	
	public  void startLoading(String msg) {
		
	    proDialog = new ProgressDialog(this);
	    proDialog.setMessage(msg);
	    proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	    proDialog.setCancelable(false);
	    proDialog.show();
	    
	}

	public  void stopLoading() {
	    proDialog.dismiss();
	    proDialog = null;
	}
	
	
	/*menu bg : 262b2f
	 * top menu button bg : 8dd2f2  
	 * 
	 * 
	 * */
	
	 boolean validationError;
	 String errorMessage;
	 
	 AutoCompleteTextView loc_autocomplete;
	 ArrayList<String> zonenames,zonenames1;
	 ArrayList<Zone_Class> zones;
	 
	 private ArrayAdapter<String> adapter;
	     
	
	 @Override
	protected void onCreate(Bundle savedInstanceState) {
	
		 super.onCreate(savedInstanceState);
		
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		setContentView(R.layout.activity_signup);
		
	//	signin_txtview=findViewById(R.id.)
		name_edit=(EditText) findViewById(R.id.edit_signup_name);
		email_edit=(EditText) findViewById(R.id.edit_signup_email);
		password_edit=(EditText) findViewById(R.id.edit_signup_password);
		loc_autocomplete=(AutoCompleteTextView)findViewById(R.id.autocompletetxt_signup_zoneid);
		
		signin_txtview=(TextView)findViewById(R.id.txt_signup_signin);
		signup_btn=(Button)findViewById(R.id.btn_signup_signup);
		
		password_edit.setTypeface(Typeface.DEFAULT);
		password_edit.setTransformationMethod(new PasswordTransformationMethod());
		
		//Get user inputs
		
		
		// Load zones
		//ArrayList<Zone_Class> zones=new ArrayList<Zone_Class>();
		//ArrayList<String> zonestring=new ArrayList<String>();
		
	zones=new ArrayList<Zone_Class>();
	zonenames=new ArrayList<String>();
	zonenames1=new ArrayList<String>();
		if(Utilities_Functions.checkinternet(getApplicationContext())){
			
			if(proDialog==null)
				startLoading("Loading. Please Wait.....");
		Ion.with(getApplicationContext())
		.load("http://egrab.qa/en/webservices/zone.php")
		.asJsonObject()
		.setCallback(new FutureCallback<JsonObject>() {
		   @Override
		    public void onCompleted(Exception e, JsonObject result) {
			   JsonArray jsonarray;
			   if(proDialog!=null)
			   stopLoading();
			   if(e==null){
		     jsonarray =result.get("items").getAsJsonArray();
		       for(int i=0;i<jsonarray.size();i++){
		    	  String zonename= jsonarray.get(i).getAsJsonObject().get("zoneName").getAsString();
		    	  String zoneid=jsonarray.get(i).getAsJsonObject().get("zoneID").getAsString();
		    	 Zone_Class z=new Zone_Class();
		    	 z.id=zoneid;
		    	 z.name=zonename;
		    	 z.trimname=zonename;
		    	z.trimname= z.trimname.replaceAll(" ", "");
		    	// Log.e("trim name", z.trimname);
		    	 zones.add(z);
		    	  zonenames.add(zonename);
		    	  zonenames1.add(zonename);
		    	  
		       }
		      
		       
		     Collections.sort(zonenames, new Comparator<String>() {
		           @Override
		           public int compare(String s1, String s2) {
		               return s1.compareToIgnoreCase(s2);
		           }
		       });
		       
		      Log.e("zones ",zonenames.toString());
		       adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.item_location,R.id.loc_item_name, zonenames)
		    		   {
				   
			       public View getView(int position, View convertView, ViewGroup parent) {
			               
			    	   	   View v = super.getView(position, convertView, parent);
			               TextView text = (TextView)v.findViewById(R.id.loc_item_name);
			               text.setTextColor(Color.BLACK);//choose your color :)     
			               
			               
			               return v;
			       }
			       
			       public View getDropDownView(int position,  View convertView,  ViewGroup parent) {
			               View v =super.getDropDownView(position, convertView, parent);
			              
			               TextView text = (TextView)v.findViewById(R.id.loc_item_name);
			               text.setTextColor(Color.BLACK);//choose your color :)              
			               return v;
			       }
			  };
			  
		  		  
		  		  loc_autocomplete.setThreshold(1);
		  		  
		  		  loc_autocomplete.setAdapter(adapter);
		 
		    	  
		       
			   }
			   else{
					Toast.makeText(getApplicationContext(), "Please Check your internet connection", Toast.LENGTH_LONG).show();
				}
		    }
		});
		}
		else{
			Toast.makeText(getApplicationContext(), "Check Your Internet connection.", Toast.LENGTH_LONG).show();
		}
		loc_autocomplete.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				loc_autocomplete.showDropDown();
			}
		});
//		loc_autocomplete.addTextChangedListener(new TextWatcher() {
//			
//			@Override
//			public void onTextChanged(CharSequence s, int start, int before, int count) {
//				// TODO Auto-generated method stub
//				 if (loc_autocomplete.enoughToFilter()) {
//		                loc_autocomplete.showDropDown();
//		                loc_autocomplete.bringToFront();
//		            }
//				
//			}
//			
//			@Override
//			public void beforeTextChanged(CharSequence s, int start, int count,
//					int after) {
//				// TODO Auto-generated method stub
//		
//				
//			}
//			
//			@Override
//			public void afterTextChanged(Editable s) {
//				// TODO Auto-generated method stub
//				
//				 String acText = loc_autocomplete.getText().toString();
//				 acText=acText.replaceAll(" ", "");
//					zonenames1.clear();
//		            for (int i=0;i<zones.size();i++) {
//
//		                if     (zones.get(i).trimname.toLowerCase().contains(acText.toLowerCase())) {
//		                	Log.e("zones to lower case", zones.get(i).trimname.toLowerCase());
//		                	Log.e("totext changed", acText.toLowerCase());
//		                    zonenames1.add(zones.get(i).name);
//		                }
//		            }
//		            Log.e("zones after filter", zonenames1.toString());
//
//		           // loc_autocomplete.setThreshold(1);
//		            loc_autocomplete.setAdapter(adapter);
//		           loc_autocomplete.showDropDown();
//			}
//		});
//		
		
		
		 loc_autocomplete.setOnItemSelectedListener( new OnItemSelectedListener() {
		       @Override
		       public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
		    	   
		    	   try{
		    		 String zonename=(String) parent.getItemAtPosition(position);
		    		 for(int i=0;i<zones.size();i++){
		    			 if(zones.get(i).name.equals(zonename)){
		    				 loc=zones.get(i).id;
		    				 loc_name=zonename;
		    				 Log.e("zone id", loc);
		    			 }
		    		 }
		    	   }
		    	   catch(Exception ex){
			    	   //Toast.makeText(getApplicationContext(), ex.toString(), Toast.LENGTH_LONG).show();
		    	   }
		    }
		      
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				
			}
	    });
		
		loc_autocomplete.setOnItemClickListener(this);
		
		
		
		//sign in
		
		signup_btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				email=email_edit.getText().toString();
				pass=password_edit.getText().toString();
				name=name_edit.getText().toString();
			//	loc=loc_edit.getText().toString();
				if(Utilities_Functions.checkinternet(getApplicationContext())){
					if(loc!=null){
						
					
				if(email.length()!=0&&pass.length()!=0&&name.length()!=0){
					if(Utilities_Functions.chekEmailId(email)){
//						Log.e("before trim", name);
//						name=name.replaceAll("\\s", " ").trim();
//						Log.e("after trim", name);
//						name=name.replaceAll(" ", "%20");
//						Log.e("after string expression", name);
//						
					startLoading("Signing up under process. Please Wait.....");
					String url="http://egrab.qa/en/webservices/register.php?firstname="+name+"&lastname=%&mobile=%&email="+email+"&password="+pass+"&zone="+loc+"&address=%";
					
					
				Ion.with(getApplicationContext())
				.load("http://egrab.qa/en/webservices/register.php")
				.setLogging("result", 2)
				.setBodyParameter("firstname", name)
				.setBodyParameter("lastname", " ")
				.setBodyParameter("mobile", " ")
				.setBodyParameter("email", email)
				.setBodyParameter("password", pass)
				.setBodyParameter("zone", loc)
				.setBodyParameter("address", " ")
				.asJsonObject()
				
				.setCallback(new FutureCallback<JsonObject>() {
				   @Override
				    public void onCompleted(Exception e, JsonObject result) {
					   stopLoading();
					   if(e==null){
						   Log.e("result", result.toString());
				       boolean login_status=result.get("success").getAsBoolean();
				       if(login_status){
				    	   ApplicationClass appState = ((ApplicationClass)getApplicationContext());
				    	   Editor editor=appState.editor;
				    	   editor.putBoolean("islogedin",true);
				    	   editor.putString("userid",email);
				    	   editor.putString("password", pass);
				    	   editor.putString("zonename", loc_name);
				    	   editor.commit();
				    	   finish();
				    	   intent=new Intent(getApplicationContext(),StoresDetails_Activity.class);
				    	 //  intent.putExtra("customerid", result.get("customerid").getAsString());
						// intent.putExtra("zone", result.get("zone").getAsString());
				    	   intent.putExtra("mode", "firstlogin");
				    	   startActivity(intent);
				    	  
				    	
				    	  // Log.e("logged in successfully", "Unable to sign up");
				    	  // finish();
				       }
				       else{
				    	   //error dialog here;
				    	   Toast.makeText(getApplicationContext(), "cant signup", Toast.LENGTH_LONG).show();
				    	   Log.e(" not logged in", "sign up");
				       }
					   }
					   else{
							Toast.makeText(getApplicationContext(), "Please Check your internet connection", Toast.LENGTH_LONG).show();
						}
					   
				    }
				});
				
				}
					else
						Toast.makeText(getApplicationContext(), "Enter a valid email id", Toast.LENGTH_LONG).show();
				
				
				}
				else{
					
		
					Toast.makeText(getApplicationContext(), "add all fields", Toast.LENGTH_LONG).show();
				}
				
				
				
				}
					else {
						Toast.makeText(getApplicationContext(), "select the avilable zones", Toast.LENGTH_LONG).show();
					}
				}
				else {
					Toast.makeText(getApplicationContext(), "Check Your Internet connection", Toast.LENGTH_LONG).show();
				}
			}
		});
		
		
		//create new account
		signin_txtview.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
				intent=new Intent(getApplicationContext(),Login_Activity.class);
				startActivity(intent);
			
			}
		});
		
		
		
		
	}
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onItemClick(AdapterView<?> item, View arg1, int position, long arg3) {
	
		 
 	   try{
 		 String zonename=(String) item.getItemAtPosition(position);
 		 for(int i=0;i<zones.size();i++){
 			 if(zones.get(i).name.equals(zonename)){
 				 loc=zones.get(i).id;
 				 loc_name=zonename;
 				 Log.e("zone id", loc);
 			 }
 		 }
 	   }
 	   catch(Exception ex){
	    	   //Toast.makeText(getApplicationContext(), ex.toString(), Toast.LENGTH_LONG).show();
 	   }
		
		
	}
}
