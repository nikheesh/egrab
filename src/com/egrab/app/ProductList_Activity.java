package com.egrab.app;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

public class ProductList_Activity extends BaseActivity {
	// declaration of variables
	TextView txt_catname, txt_subcatname, txt_noofcarts;

	private String[] navMenuTitles;
	private TypedArray navMenuIcons;
	private DrawerLayout mDrawerLayout;
	public boolean mSlideState = false;

	ImageButton imgbtn_menu, imgbtn_cart, imgbtn_search, img_closesearch;
	ImageView img_logo;
	Intent intent;
	GridView prod_gridview;
	ArrayAdapter<JsonObject> productAdapter;
	String storeid, customerid, subcatid, catid;
	int pageno;
	int lastcount;
	int noofcart;
	SharedPreferences pref;
	Editor editor;
	EditText edit_search;
	RelativeLayout ll_edit_search;
	boolean search;
	LinearLayout lnr_add_layout;
	Button btn_add_more;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.activity_productlist);
		getActionBar().hide();

		navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);
		navMenuIcons = getResources()
				.obtainTypedArray(R.array.nav_drawer_icons);

		set(navMenuTitles, navMenuIcons);

		mSlideState = false;

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

		mDrawerLayout.setDrawerListener(new ActionBarDrawerToggle(this,
				mDrawerLayout, R.drawable.ic_launcher, 0, 0) {

			@Override
			public void onDrawerClosed(View drawerView) {
				super.onDrawerClosed(drawerView);
				mSlideState = false;// is Closed
			}

			@Override
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				mSlideState = true;// is Opened
			}
		});

		lastcount = 0;

		ApplicationClass appState = ((ApplicationClass) getApplicationContext());

		pref = appState.pref;
		editor = appState.editor;

		txt_noofcarts = (TextView) findViewById(R.id.txt_productlist_noofcart);

		edit_search = (EditText) findViewById(R.id.edit_productlist_search);
		ll_edit_search = (RelativeLayout) findViewById(R.id.ll_productlist_search);
		lnr_add_layout = (LinearLayout) findViewById(R.id.lnr_add_layout);
		btn_add_more = (Button) findViewById(R.id.btn_add_more);
		btn_add_more.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// Toast.makeText(ProductList_Activity.this, "clickd",
				// 1).show();
				startActivity(new Intent(ProductList_Activity.this,
						Temporary/* AddExtraItems */.class));
			}
		});
		edit_search
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {
						if (actionId == EditorInfo.IME_ACTION_SEARCH) {

							// Toast.makeText(getApplicationContext(),
							// "click got called",Toast.LENGTH_SHORT).show();
							doSearchProduct();
							InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
							imm.hideSoftInputFromWindow(
									edit_search.getWindowToken(), 0);

							return true;
						}
						return false;
					}
				});

		productAdapter = new ArrayAdapter<JsonObject>(this, 0) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				// load contents on swiping up as only 20 contents are loaded
				// initially

				if ((position > getCount() - 4) && lastcount == 10
						&& search == false)
					loadurl(storeid, customerid, catid, subcatid);

				if (convertView == null)
					convertView = getLayoutInflater().inflate(
							R.layout.productlist_item, null);

				final JsonObject json = getItem(position);
				Log.i("Adapter  length", "" + getCount());

				RelativeLayout rl = (RelativeLayout) convertView
						.findViewById(R.id.rl_productlist_product);
				TextView txt_prodname = (TextView) convertView
						.findViewById(R.id.txt_productlist_prodname);
				TextView txt_companyname = (TextView) convertView
						.findViewById(R.id.txt_productlist_companyname);
				TextView txt_price = (TextView) convertView
						.findViewById(R.id.txt_productlist_price);
				TextView txt_noofprod = (TextView) convertView
						.findViewById(R.id.txt_productlist_noofprod);

				Button btn_addtocart = (Button) convertView
						.findViewById(R.id.btn_productlist_addtocart);
				ImageView img_prod = (ImageView) convertView
						.findViewById(R.id.img_productlist_prodimg);

				txt_prodname.setText(json.get("description").getAsString());
				txt_companyname.setText(json.get("prodname").getAsString());
				txt_price.setText(json.get("pricecode").getAsString());

				final String pimg = "http://www.egrab.qa/en/products/"
						+ json.get("resizeimg").getAsString();

				Ion.with(img_prod).placeholder(R.drawable.loading_def_product)
						.error(R.drawable.loading_def_product).load(pimg);
				String pid = json.get("prodid").getAsString();
				if (pref.contains(pid)) {
					txt_noofprod.setVisibility(View.VISIBLE);
					txt_noofprod.setText("" + pref.getInt(pid, 0));
				} else {
					txt_noofprod.setVisibility(View.GONE);
				}

				btn_addtocart.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent i = new Intent(getApplicationContext(),
								Add_Cart_Activity.class);
						i.putExtra("pid", json.get("prodid").getAsString());
						i.putExtra("pname", json.get("description")
								.getAsString());
						i.putExtra("pcompany", json.get("prodname")
								.getAsString());
						i.putExtra("pimg", pimg);
						i.putExtra("pprice", json.get("pricecode")
								.getAsString());
						i.putExtra("storeid", storeid);
						i.putExtra("customerid", customerid);
						i.putExtra("subcatid", subcatid);
						startActivity(i);

					}
				});

				rl.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent i = new Intent(getApplicationContext(),
								Add_Cart_Activity.class);
						i.putExtra("pid", json.get("prodid").getAsString());
						i.putExtra("pname", json.get("description")
								.getAsString());
						i.putExtra("pcompany", json.get("prodname")
								.getAsString());
						i.putExtra("pimg", pimg);
						i.putExtra("pprice", json.get("pricecode")
								.getAsString());
						i.putExtra("storeid", storeid);
						i.putExtra("customerid", customerid);
						i.putExtra("subcatid", subcatid);
						startActivity(i);
					}
				});

				return convertView;
			}
		};

		txt_catname = (TextView) findViewById(R.id.txt_productlist_catname);
		txt_subcatname = (TextView) findViewById(R.id.txt_productlist_subcatname);
		imgbtn_cart = (ImageButton) findViewById(R.id.imgbtn_productlist_cart);
		imgbtn_menu = (ImageButton) findViewById(R.id.imgbtn_productlist_menulist);
		imgbtn_search = (ImageButton) findViewById(R.id.imgbtn_productlist_search);
		img_closesearch = (ImageButton) findViewById(R.id.imgbtn_productlist_close);
		edit_search = (EditText) findViewById(R.id.edit_productlist_search);
		img_logo = (ImageView) findViewById(R.id.img_productlist_logo);

		prod_gridview = (GridView) findViewById(R.id.gridview_productlist_products);
		prod_gridview.setAdapter(productAdapter);
		
		
		prod_gridview.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// TODO Auto-generated method stub
				
				

				if (prod_gridview.getLastVisiblePosition() == prod_gridview
						.getCount() - 1) {
					Animation animFadeIn = AnimationUtils.loadAnimation(
							getApplicationContext(), android.R.anim.fade_in);
					lnr_add_layout.setAnimation(animFadeIn);

					lnr_add_layout.setVisibility(View.VISIBLE);
					if (lnr_add_layout.getVisibility() == View.VISIBLE) {
						lnr_add_layout.postDelayed(new Runnable() {
							public void run() {
								Log.i("Inside", "Handler");
								Animation animFadeOut = AnimationUtils
										.loadAnimation(getApplicationContext(),
												android.R.anim.fade_out);
								lnr_add_layout.setAnimation(animFadeOut);
								lnr_add_layout.setVisibility(View.GONE);
							}
						}, 10000);
					}
				} else {
					lnr_add_layout.setVisibility(View.GONE);
				}
				
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, final int totalItemCount) {
				// TODO Auto-generated method stub

				
				
				 

			}
		});

		txt_catname.setText(getIntent().getStringExtra("catname"));
		txt_subcatname.setText(getIntent().getStringExtra("subcatname"));
		subcatid = getIntent().getStringExtra("subcatid");
		catid = getIntent().getStringExtra("catid");
		search = false;
		// which prod api uses for this
		storeid = getIntent().getStringExtra("storeid");
		// ApplicationClass appState =
		// ((ApplicationClass)getApplicationContext());

		pref = appState.pref;
		customerid = pref.getString("customerid", "0");

		pageno = 1;

		txt_catname.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// intent=new
				// Intent(getApplicationContext(),Categories_Activity.class);
				// startActivity(intent);
				finish();
			}
		});
		txt_subcatname.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// intent=new
				// Intent(getApplicationContext(),Categories_Activity.class);
				// startActivity(intent);
				finish();
			}
		});

		imgbtn_cart.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent i = new Intent(getApplicationContext(),
						Cart_Activity.class);
				startActivity(i);
			}
		});

	}

	

	



	public void loadurl(String storeid, String customerid, String catid,
			String subcatid) {
		// http://egrab.qa/en/webservices/productbysubcategory.php?storeid=%@&categoryid=%@&subcategoryid=%@&customerid=%@&offset=%@
		if (Utilities_Functions.checkinternet(getApplicationContext())) {
			String urlprodlist = "http://egrab.qa/en/webservices/productbysubcategory.php?storeid="
					+ storeid
					+ "&categoryid="
					+ catid
					+ "&subcategoryid="
					+ subcatid
					+ "&customerid="
					+ customerid
					+ "&offset="
					+ pageno;
			Ion.with(getApplicationContext()).load(urlprodlist).asJsonObject()
					.setCallback(new FutureCallback<JsonObject>() {
						@Override
						public void onCompleted(Exception e, JsonObject result) {
							if (e == null) {
								boolean status = result.get("success")
										.getAsBoolean();
								if (status) {

									JsonArray jsonarray = result.get("items")
											.getAsJsonArray();
									lastcount = jsonarray.size();
									for (int i = 0; i < jsonarray.size(); i++) {
										JsonObject jsonobj = jsonarray.get(i)
												.getAsJsonObject();
										productAdapter.add(jsonobj);
										pageno++;
									}

								} else {
									// error dialog here;
									Log.e(" not logged in", "sign up");
								}
							} else {
								Toast.makeText(
										getApplicationContext(),
										"Please Check your internet connection",
										Toast.LENGTH_LONG).show();

							}
						}
					});
		} else {
			Toast.makeText(getApplicationContext(),
					"Please Check your internet connection", Toast.LENGTH_LONG)
					.show();
		}

	}

	public void loadurl(String tag) {
		// http://egrab.qa/en/webservices/productbysubcategory.php?storeid=%@&categoryid=%@&subcategoryid=%@&customerid=%@&offset=%@
		if (Utilities_Functions.checkinternet(getApplicationContext())) {
			String urlprodlist = "http://www.egrab.qa/en/webservices/searchbystore.php?storeid="
					+ storeid + "&productname=" + tag;
			Ion.with(getApplicationContext()).load(urlprodlist).asJsonObject()
					.setCallback(new FutureCallback<JsonObject>() {
						@Override
						public void onCompleted(Exception e, JsonObject result) {
							if (e == null) {
								boolean status = result.get("success")
										.getAsBoolean();
								if (status) {

									JsonArray jsonarray = result.get("items")
											.getAsJsonArray();
									lastcount = jsonarray.size();
									for (int i = 0; i < jsonarray.size(); i++) {
										JsonObject jsonobj = jsonarray.get(i)
												.getAsJsonObject();
										productAdapter.add(jsonobj);
										pageno++;

									}

								} else {
									// error dialog here;
									Log.e(" not logged in", "sign up");
								}
							} else {
								Toast.makeText(
										getApplicationContext(),
										"Please Check your internet connection",
										Toast.LENGTH_LONG).show();

							}

						}
					});
		} else {
			Toast.makeText(getApplicationContext(),
					"Please Check your internet connection", Toast.LENGTH_LONG)
					.show();
		}

	}

	public void searchProduct(View v) {

		// search=true;

		// Toast.makeText(getApplicationContext(),
		// "inside search function",Toast.LENGTH_SHORT).show();

		// if(edit_search.getVisibility()==View.GONE){
		//
		ll_edit_search.setVisibility(View.VISIBLE);
		edit_search.setFocusable(true);
		img_logo.setVisibility(View.GONE);
		imgbtn_search.setVisibility(View.GONE);
		// img_closesearch.setVisibility(View.VISIBLE);

		// Toast.makeText(getApplicationContext(),
		// "inside search function negative block",Toast.LENGTH_SHORT).show();
		//
		// }
		// else{

		// Toast.makeText(getApplicationContext(),
		// "inside search function positive block",Toast.LENGTH_SHORT).show();

		// String tag=edit_search.getText().toString();
		// if(Utilities_Functions.checkinternet(getApplicationContext())){
		//
		// txt_catname.setOnClickListener(new OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// // TODO Auto-generated method stub
		// // intent=new
		// Intent(getApplicationContext(),Categories_Activity.class);
		// // startActivity(intent);
		// finish();
		// }
		// });
		//
		// txt_subcatname.setOnClickListener(new OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// // TODO Auto-generated method stub
		// // intent=new
		// Intent(getApplicationContext(),Categories_Activity.class);
		// // startActivity(intent);
		// finish();
		// }
		// });
		//
		// txt_catname.setText("Search");
		// txt_subcatname.setText("Results");
		//
		// productAdapter.clear();
		// productAdapter.notifyDataSetChanged();
		// pageno=1;
		// loadurl(tag);
		// }
		// else{
		// Toast.makeText(getApplicationContext(),
		// "Please Check your internet connection", Toast.LENGTH_LONG).show();
		// }

	}

	public void closeSearch(View v) {
		ll_edit_search.setVisibility(View.GONE);
		img_logo.setVisibility(View.VISIBLE);
		imgbtn_search.setVisibility(View.VISIBLE);

		search = false;
		txt_catname.setText(getIntent().getStringExtra("catname"));
		txt_subcatname.setText(getIntent().getStringExtra("subcatname"));

		txt_catname.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// intent=new
				// Intent(getApplicationContext(),Categories_Activity.class);
				// startActivity(intent);
				finish();
			}
		});

		txt_subcatname.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// intent=new
				// Intent(getApplicationContext(),Categories_Activity.class);
				// startActivity(intent);
				finish();
			}
		});
		// img_closesearch.setVisibility(View.GONE);

	}

	@Override
	public void onResume() {
		super.onResume();
		// put your code here...
		if (pref.contains("noofcart")) {
			txt_noofcarts.setText("" + pref.getInt("noofcart", 0));
			txt_noofcarts.setVisibility(View.VISIBLE);
		} else {
			txt_noofcarts.setVisibility(View.GONE);
		}
		if (productAdapter.getCount() <= 0) {
			productAdapter.clear();
			productAdapter.notifyDataSetChanged();
			pageno = 1;
			loadurl(storeid, customerid, catid, subcatid);

		} else {
			productAdapter.notifyDataSetChanged();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void showNavigationBar(View view) {

		if (mSlideState) {
			mDrawerLayout.closeDrawer(Gravity.LEFT);
		} else {
			mDrawerLayout.openDrawer(Gravity.LEFT);
		}

	}

	public void clk_addButton(View view) {

		RelativeLayout rl = (RelativeLayout) findViewById(R.id.rl_productlist_product);
		rl.performClick();
	}

	public void doSearchProduct() {

		search = true;

		// Toast.makeText(getApplicationContext(),
		// "inside search function",Toast.LENGTH_SHORT).show();

		String tag = edit_search.getText().toString();
		if (Utilities_Functions.checkinternet(getApplicationContext())) {

			txt_catname.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// intent=new
					// Intent(getApplicationContext(),Categories_Activity.class);
					// startActivity(intent);
					finish();
				}
			});

			txt_subcatname.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// intent=new
					// Intent(getApplicationContext(),Categories_Activity.class);
					// startActivity(intent);
					finish();
				}
			});

			txt_catname.setText("Search");
			txt_subcatname.setText("Results");

			productAdapter.clear();
			productAdapter.notifyDataSetChanged();
			pageno = 1;
			loadurl(tag);
		} else {
			Toast.makeText(getApplicationContext(),
					"Please Check your internet connection", Toast.LENGTH_LONG)
					.show();
		}

	}

}