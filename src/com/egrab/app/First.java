package com.egrab.app;



import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

public class First  extends BaseActivity {
	
	private String[] navMenuTitles;
	private TypedArray navMenuIcons;
	private DrawerLayout mDrawerLayout;
	public boolean mSlideState=false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.first);
		
		getActionBar().hide();
		
		navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);
		navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);
		
		set(navMenuTitles,navMenuIcons);
		
		mSlideState = false;
		
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
				
		mDrawerLayout.setDrawerListener(new ActionBarDrawerToggle(this,mDrawerLayout, R.drawable.ic_launcher, 0, 0){
			@Override
			public void onDrawerClosed(View drawerView) {                       
			    super.onDrawerClosed(drawerView);
			    mSlideState=false;//is Closed
			}
			
			@Override
			public void onDrawerOpened(View drawerView) {                       
			    super.onDrawerOpened(drawerView);
			    mSlideState=true;//is Opened
			}});
		
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void doSomething(View view){
		
		Toast.makeText(getApplicationContext(), "TOASTING FROM INSDIE BUTTON CLICK", Toast.LENGTH_SHORT).show();
		
		if(mSlideState){                
		    mDrawerLayout.closeDrawer(Gravity.LEFT);
		}else{              
		    mDrawerLayout.openDrawer(Gravity.LEFT);
		}
		
	}
}
