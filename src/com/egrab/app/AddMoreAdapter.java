package com.egrab.app;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class AddMoreAdapter extends BaseAdapter {
	Context context;
	static ArrayList<HashMap<String, String>> arr = new ArrayList<HashMap<String, String>>();
	DbHelper db_helper;

	public AddMoreAdapter(Context context,
			ArrayList<HashMap<String, String>> arr) {
		this.context = context;
		this.arr = arr;
		this.db_helper = new DbHelper(context);

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arr.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(final int pos, View convertview, ViewGroup parent) {
		// TODO Auto-generated method stub
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View row = convertview;
		ViewHolder holder = null;

		if (row == null) {
			row = inflater.inflate(R.layout.segmented_list_row, parent, false);
			holder = new ViewHolder();
			holder.tv_add_name = (TextView) row.findViewById(R.id.tv_add_name);
			holder.tv_add_cat = (TextView) row.findViewById(R.id.tv_add_cat);
			holder.tv_add_qty = (TextView) row.findViewById(R.id.tv_add_qty);
			holder.imv_del_extra = (ImageView) row
					.findViewById(R.id.imv_del_extra);

			row.setTag(holder);
		} else {
			holder = (ViewHolder) row.getTag();
		}
		holder.tv_add_name.setText(arr.get(pos).get("name"));
		holder.tv_add_cat.setText(arr.get(pos).get("category"));
		holder.tv_add_qty.setText(arr.get(pos).get("qty"));
		holder.imv_del_extra.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Log.i("image", "deletewed");
				boolean status = db_helper.deleteProduct(arr.get(pos).get(
						"name"));
				Log.i("status from deletion", "" + status);
				if (status) {
					arr.remove(pos);
					Toast.makeText(context, "Deleted", 1000).show();
				}

				notifyDataSetChanged();

			}
		});

		return row;
	}

	static class ViewHolder {
		TextView tv_add_name, tv_add_cat, tv_add_qty, tv_sr_num;
		ImageView imv_del_extra;

	}

}
