package com.egrab.app;



import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

public class Alert_Dialog_Activity extends Activity {
	SharedPreferences pref;
	Editor editor;
	String custid;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.alert_dialog_xml);
		
		 final ApplicationClass appState = ((ApplicationClass)getApplicationContext());
		 pref=appState.pref;
		 editor=appState.editor;
		 custid=pref.getString("customerid", "0");

		 
		 
		   ImageButton closeButton=(ImageButton)findViewById(R.id.imgbtn_alert_close);
           closeButton.setOnClickListener(new  OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent i=new Intent(getApplicationContext(),StoresDetails_Activity.class);
					i.putExtra("mode", "secondlogin");
					startActivity(i);
					finish();
				}
			});
          
           Button dialogButton = (Button) findViewById(R.id.btn_alert_exploreeGrab);

           dialogButton.setOnClickListener(new OnClickListener() {
         @Override

               public void onClick(View v) {

               //  appState.editor.putString("zonename", "eGrab Plus");
        	 ApplicationClass appState = ((ApplicationClass)getApplicationContext());
         	String zone=appState.pref.getString("zone","");
               String storeid="11";
                 Intent intent=new Intent(getApplicationContext(),Categories_Activity.class);
                 intent.putExtra("zone", zone);
                 intent.putExtra("storeid", storeid);
                
                 startActivity(intent);
                 finish();
                 
               }

           });
           Button btn_changeLoc=(Button)findViewById(R.id.btn_alert_change_loc);
           btn_changeLoc.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			
				if(Utilities_Functions.checkinternet(getApplicationContext())){
					finish();
					Intent i=new Intent(getApplicationContext(),ZoneList_Activity.class);
					i.putExtra("customerid", custid);
					editor.remove("zonename");
					editor.commit();
					startActivity(i);
					}
					else
					{
						Toast.makeText(getApplicationContext(), "Please Check your internet connection", Toast.LENGTH_LONG).show();
					}
			}
		});
          
		
	}
}
