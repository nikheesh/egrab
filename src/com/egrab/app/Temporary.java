package com.egrab.app;
// Class is for adding Special Requests to the Cart
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

public class Temporary extends BaseActivity {
	// declaration of variables
	TextView txt_location, txt_noofcarts, txt_loc_change, txt_noitems;

	private String[] navMenuTitles;
	private TypedArray navMenuIcons;
	private DrawerLayout mDrawerLayout;
	public boolean mSlideState = false;
	public static String ad_image_url;
	ImageButton imgbtn_menu, imgbtn_cart, imgbt_close;
	Intent intent;
	ImageView imv_advertisement;
	ListView store_listview;
	ArrayAdapter<JsonObject> storeAdapter;
	// ArrayList<Map<String,String>> zones;
	SharedPreferences pref;
	Editor editor;
	String zoneid, custid;
	Button btn_add_to_cart1;
	public ProgressDialog proDialog;
	DbHelper db_helper;
	EditText edt_name, edt_size, edt_qty, edt_notes;

	public void startLoading(String msg) {
		proDialog = new ProgressDialog(this);
		proDialog.setMessage(msg);
		proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		proDialog.setCancelable(false);
		proDialog.show();
	}

	public void stopLoading() {
		proDialog.dismiss();
		proDialog = null;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		try {
			// this.requestWindowFeature(Window.FEATURE_NO_TITLE);
			setContentView(R.layout.temp);
			db_helper = new DbHelper(Temporary.this);
			edt_name = (EditText) findViewById(R.id.edt_name);
			edt_size = (EditText) findViewById(R.id.edt_size);
			edt_qty = (EditText) findViewById(R.id.edt_qty);
			edt_notes = (EditText) findViewById(R.id.edt_notes);
			btn_add_to_cart1 = (Button) findViewById(R.id.btn_add_to_cart1);
			getActionBar().hide();

			navMenuTitles = getResources().getStringArray(
					R.array.nav_drawer_items);
			navMenuIcons = getResources().obtainTypedArray(
					R.array.nav_drawer_icons);

			set(navMenuTitles, navMenuIcons);

			mSlideState = false;

			mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

			mDrawerLayout.setDrawerListener(new ActionBarDrawerToggle(this,
					mDrawerLayout, R.drawable.ic_launcher, 0, 0) {

				@Override
				public void onDrawerClosed(View drawerView) {
					super.onDrawerClosed(drawerView);
					mSlideState = false;// is Closed
				}

				@Override
				public void onDrawerOpened(View drawerView) {
					super.onDrawerOpened(drawerView);
					mSlideState = true;// is Opened
				}
			});
			imgbt_close = (ImageButton) findViewById(R.id.imgbt_close);
			imgbt_close.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					finish();
				}
			});
			btn_add_to_cart1.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (edt_name.getText().toString().compareToIgnoreCase("") == 0) {
						Toast.makeText(Temporary.this, "Product Name missing!",
								1000).show();
					}

					else if (edt_size.getText().toString()
							.compareToIgnoreCase("") == 0) {
						Toast.makeText(Temporary.this, "Size Missing!", 1000)
								.show();
					} else if (edt_qty.getText().toString()
							.compareToIgnoreCase("") == 0) {
						Toast.makeText(Temporary.this, "Quantity Missing!",
								1000).show();
					}

					else {
						db_helper.insertProducts(edt_name.getText().toString(),
								edt_size.getText().toString(), edt_qty
										.getText().toString(), edt_notes
										.getText().toString());
						edt_name.setText("");
						edt_size.setText("");

						edt_qty.setText("");

						edt_notes.setText("");
						Toast.makeText(Temporary.this, "Product added", 1000)
								.show();
					}

				}
			});

		} catch (Exception e) {

		}

	}

	@Override
	public void onBackPressed() {
		finish();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	public void showNavigationBar(View view) {

		if (mSlideState) {
			mDrawerLayout.closeDrawer(Gravity.LEFT);
		} else {
			mDrawerLayout.openDrawer(Gravity.LEFT);
		}

	}

}