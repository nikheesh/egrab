package com.egrab.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class CompletedOrder_Activity extends Activity{
	TextView txt_refno;
	Button btn_backtomain;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_completedorder);
		
		txt_refno=(TextView)findViewById(R.id.txt_completedorder_refno);
		btn_backtomain=(Button)findViewById(R.id.btn_completedorder_backtomainmenu);
		String orderid=getIntent().getStringExtra("orderid");
		txt_refno.setText(orderid);
		
		btn_backtomain.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),StoresDetails_Activity.class);
				startActivity(i);
				finish();
			}
		});
		
		
	}

}
