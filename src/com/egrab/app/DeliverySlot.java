package com.egrab.app;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import jp.dip.sys1.android.drumpicker.lib.DrumPicker;
import jp.dip.sys1.android.drumpicker.lib.TimeDrumPicker;
import jp.dip.sys1.android.drumpicker.lib.DrumPicker.OnPositionChangedListener;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class DeliverySlot extends Activity {
	public static int index_of_list, index_of_list_copy;
	List<String> hourList;
	int hour_12;
	int hour_24;
	Button del_btn;
	SharedPreferences pref;
	Editor editor;
	public ProgressDialog proDialog;
	DbHelper db_helper;

	static String time_for_bckend;
	String str_cart_extra_item = "::;";

	public void startLoading() {
		proDialog = new ProgressDialog(this);
		proDialog.setMessage("Please Wait.....");
		proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		proDialog.setCancelable(false);
		proDialog.show();
	}

	public void stopLoading() {
		proDialog.dismiss();
		proDialog = null;
	}

	DrumPicker tPicker1, timepicker2;
	TextView tv_day;
	final String[] HOUR = { "9am-10am", "10am-11am", "11am-12am", "12am-1pm",
			"1pm-2pm", "2pm-3pm", "3pm-4pm", "4pm-5pm", "5pm-6pm", "6pm-7pm",
			"7pm-8pm", "8pm-9pm", "9pm-10pm", "10pm-11pm" };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.set_time_slot);
		ApplicationClass appState = ((ApplicationClass) getApplicationContext());

		pref = appState.pref;
		editor = appState.editor;

		hourList = new ArrayList<String>(Arrays.asList(HOUR));
		Calendar calendar = Calendar.getInstance();
		final int year = calendar.get(Calendar.YEAR);
		final int month = calendar.get(Calendar.MONDAY);
		final int day = calendar.get(Calendar.DAY_OF_MONTH);
		final int hour = calendar.get(Calendar.HOUR_OF_DAY);
		final int minitue = calendar.get(Calendar.MINUTE);
		hour_12 = calendar.get(Calendar.HOUR);
		hour_24 = calendar.get(Calendar.HOUR_OF_DAY);
		tv_day = (TextView) findViewById(R.id.tv_day);
		del_btn = (Button) findViewById(R.id.del_btn);
		tPicker1 = (DrumPicker) findViewById(R.id.timepicker1);
		db_helper = new DbHelper(DeliverySlot.this);
		ArrayList<HashMap<String, String>> extra_items = db_helper
				.getAllProducts();
		String append = "::;";
		for (int i = 0; i < extra_items.size(); i++) {
			if (i == extra_items.size() - 1) {
				append = "";
			} else {
				append = "::;";
			}
			str_cart_extra_item = str_cart_extra_item + "prodname:"
					+ extra_items.get(i).get("name") + ",brand:"
					+ extra_items.get(i).get("category") + ",quantity:"
					+ extra_items.get(i).get("qty") + ",desc:"
					+ extra_items.get(i).get("desc") + append;
		}

		Log.i("str_cart_extra_item", Confirm_Activity.notes
				+ ".Get delivered at:" + tv_day.getText().toString() + ","
				+ time_for_bckend + str_cart_extra_item);
		tPicker1.addRow(HOUR, 200);
		// tPicker1.addRow(hourList, 100);
		del_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				final String url = "http://egrab.qa/en/webservices/order.php";
				startLoading();
				Ion.with(getApplicationContext())
						.load(url)
						.setBodyParameter("customerid", Confirm_Activity.custid)
						.setBodyParameter("customer_address",
								Confirm_Activity.addr)
						.setBodyParameter("customer_zone",
								Confirm_Activity.zone)
						.setBodyParameter("mobile_number",
								Confirm_Activity.phone)
						.setBodyParameter("streat", Confirm_Activity.streetno)
						.setBodyParameter("building_number",
								Confirm_Activity.buildno)
						.setBodyParameter(
								"order_notes",
								Confirm_Activity.notes + ".Get delivered at:"
										+ tv_day.getText().toString() + ","
										+ time_for_bckend + str_cart_extra_item)
						.setBodyParameter("lat", "" + Confirm_Activity.lat)
						.setBodyParameter("lng", "" + Confirm_Activity.lng)
						.asJsonObject()
						.setCallback(new FutureCallback<JsonObject>() {
							@Override
							public void onCompleted(Exception e,
									JsonObject result) {
								if (e == null) {

									boolean status = result.get("success")
											.getAsBoolean();
									Log.i("Delivery status error", "" + status);
									if (status) {
										JsonObject json = result.get("items")
												.getAsJsonObject();
										String orderid = Integer.toString(json
												.get("orderid").getAsInt());
										stopLoading();
										if (Confirm_Activity.listofpid != null) {
											for (int i = 0; i < Confirm_Activity.listofpid
													.size(); i++) {
												editor.remove(Confirm_Activity.listofpid
														.get(i));
												editor.commit();
												Log.e(Confirm_Activity.listofpid
														.get(i), "removed");
											}
										}

										editor.remove("noofcart");
										editor.commit();

										Intent i = new Intent(
												getApplicationContext(),
												CompletedOrder_Activity.class);
										i.putExtra("orderid", orderid);
										startActivity(i);

									} else {
										stopLoading();
										Toast.makeText(
												getApplicationContext(),
												"your order can not be confirm now",
												Toast.LENGTH_LONG).show();
									}
								} else {
									stopLoading();
									Log.e("internet   url  ", url + "");
									Toast.makeText(
											getApplicationContext(),
											"Please Check your internet connection",
											Toast.LENGTH_LONG).show();
								}

							}

						});
			}
		});
		tPicker1.setOnPostionChangedListener(new OnPositionChangedListener() {

			@Override
			public void onPositionChanged(int arg0, int pos) {
				// TODO Auto-generated method stub
				Log.i("position of drummer", "" + pos);
				int pos_copy = pos;

				if (minitue < 30) {
					pos_copy = pos - 1;
				} else {
					pos_copy = pos - 2;
				}

				if (pos_copy < index_of_list) {
					tv_day.setText("Tomorrow");
				} else {
					tv_day.setText("Today");
				}
			}

		});

		init();
		tv_day.setText("Today");

	}

	private void init() {
		// TODO Auto-generated method stub
		if (hour_24 >= 0 && hour_24 <= 12) {
			// am slot
			if (hour_12 < 9) {
				tPicker1.setPosition(0, 1);
				index_of_list = 1;
				time_for_bckend = HOUR[1];
			} else {
				index_of_list = hourList.indexOf(String.valueOf(hour_12)
						+ "am-" + String.valueOf(hour_12 + 1) + "am");
				check_time(index_of_list);
				time_for_bckend = HOUR[index_of_list];
				Log.i("timesssssssssssss 1", String.valueOf(hour_12) + "am-"
						+ String.valueOf(hour_12 + 1) + "am");
			}

		} else {
			// pm slot
			if (hour_12 == 11) {
				tPicker1.setPosition(0, 4);
				index_of_list = 4;
				time_for_bckend = HOUR[4];
			} else if (hour_12 == 0) {
				tPicker1.setPosition(0, 6);
				index_of_list = 6;
				time_for_bckend = HOUR[6];
			} else {

				index_of_list = hourList.indexOf(String.valueOf(hour_12)
						+ "pm-" + String.valueOf(hour_12 + 1) + "pm");
				check_time(index_of_list);
				time_for_bckend = HOUR[index_of_list];
				Log.i("timesssssssssssss 1", String.valueOf(hour_12) + "pm-"
						+ String.valueOf(hour_12 + 1) + "pm");
			}
		}
		//
		// if (hour_24 >= 0 && hour_24 <= 12) {
		// if (hour_12 == 0) {
		// index_of_list = hourList.indexOf(String.valueOf(hour_12 + 1)
		// + "pm-" + String.valueOf(hour_12 + 2) + "pm");
		// check_time(index_of_list);
		// Log.i("timesssssssssssss 1", String.valueOf(hour_12) + "am-"
		// + String.valueOf(hour_12 + 1) + "am");
		//
		// } else if (hour_12 + 1 > 12) {
		// index_of_list = hourList.indexOf("1pm-2pm");
		// check_time(index_of_list);
		// Log.i("timesssssssssssss 2 ", "1pm-2pm");
		//
		// } else {
		// index_of_list = hourList.indexOf(String.valueOf(hour_12)
		// + "am-" + String.valueOf(hour_12 + 1) + "am");
		// check_time(index_of_list);
		// Log.i("timesssssssssssss 3", String.valueOf(hour_12) + "am-"
		// + String.valueOf(hour_12 + 1) + "am");
		//
		// }
		//
		// } else if (hour_24 > 12 && hour_24 <= 24) {
		// index_of_list = hourList.indexOf(String.valueOf(hour_12) + "pm-"
		// + String.valueOf(hour_12 + 1) + "pm");
		// check_time(index_of_list);
		// Log.i("timesssssssssssss 4", String.valueOf(hour_12) + "pm-"
		// + String.valueOf(hour_12 + 1) + "pm");
		//
		// }

	}

	private void check_time(int index) {
		// TODO Auto-generated method stub
		Log.i("index", "" + index + ",,,,hr" + HOUR.length + 1);
		// tPicker1.setPosition(0, 14);
		Calendar calendar = Calendar.getInstance();

		int mins = calendar.get(Calendar.MINUTE);
		Log.i("Minutes from check_time", "" + mins);

		if (mins < 30) {
			index = index + 1;
		} else {
			index = index + 2;
		}

		

		if (index <= HOUR.length) {
			Log.i("index++", "" + index + ",,,,hr" + HOUR.length + ",,,,"
					+ HOUR[index]);
			tPicker1.setPosition(0, index);
			time_for_bckend = HOUR[index];
		}

	}

}
