package com.egrab.app;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

public class ExpandableListAdapter extends BaseExpandableListAdapter implements Filterable{

    private LayoutInflater inflater;
    private ArrayList<ExpandableListParentClass> mParent;
    private View view;
    Context context;
    
    public ArrayList<ExpandableListParentClass> getMParent() {
        return mParent;    }



    public ExpandableListAdapter(Context context,ArrayList<ExpandableListParentClass> parentList ) {
        this.mParent = parentList;
        this.inflater = LayoutInflater.from(context);
        this.context=context;

    }

    // counts the number of group/parent items so the list knows how many
    // times calls getGroupView() method
@Override
    public int getGroupCount() {
        return mParent.size();
    }

    // counts the number of children items so the list knows how many times
    // calls getChildView() method
@Override
    public int getChildrenCount(int parentPosition) {
        int size =0;
        if(mParent.get(parentPosition).getParentChildren() != null){
        size = mParent.get(parentPosition).getParentChildren().size();
        }
        return size;
    }

    // gets the title of each parent/group
@Override
    public Category_Class getGroup(int i) {
        return mParent.get(i).getParent();
    }

    // gets the name of each item
@Override
    public Category_Class getChild(int parentPosition, int childPosition) {
        return mParent.get(parentPosition).getParentChildren().get(childPosition);
    }
@Override
    public long getGroupId(int parentPosition) {
        return parentPosition;
    }
@Override
    public long getChildId(int i, int childPosition) {
        return childPosition;
    }
@Override
    public boolean hasStableIds() {
        return true;
    }

    // in this method you must set the text to see the parent/group on the list
@Override
    public View getGroupView(int parentPosition, boolean b, View view, ViewGroup viewGroup) {
     
         if (view == null) {
             LayoutInflater infalInflater = (LayoutInflater) context
                     .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
             view = infalInflater.inflate(R.layout.category_maingroup_item, null);
         }
       final Category_Class catclass=this.getGroup(parentPosition);
       
         TextView lblListHeader = (TextView) view.findViewById(R.id.txt_category_maincategoryname);
         lblListHeader.setTypeface(null, Typeface.NORMAL);
         lblListHeader.setText(catclass.Categoryname);
         
         
         final View viewitem=view;
         final int parentpos=parentPosition;
            
         return view;
    }

    // in this method you must set the text to see the children on the list
@Override
    public View getChildView(int parentPosition, int childPosition, boolean b, View view, ViewGroup viewGroup) {
    	 
         if (view == null) {
             LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
             view = infalInflater.inflate(R.layout.category_subgroup_item, null);
         }
       
         final Category_Class catclass = getChild(parentPosition, childPosition);
     	
         TextView txtListChild = (TextView) view
                 .findViewById(R.id.txt_category_subcategoryname);
  
         txtListChild.setText(catclass.Categoryname);
        
         
         return view;


        // return the entire view
        
    }
@Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }


@Override
public Filter getFilter() {
	// TODO Auto-generated method stub
	return null;
}
public ExpandableListAdapter getAdapter(){
	return this;
}

    }


 