package com.egrab.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;


public class Add_Cart_Activity extends Activity {
	//declaration of variables
	
	TextView txt_prodname,txt_companyname,txt_price,txt_qty;
	Button btn_addtocart,addqty,subqty;
	ImageButton imgbtn_close;
	ImageView img_prod;
	int count;
	int noofcart;
	SharedPreferences pref;
	Editor editor;
	String storeid,customerid,subcatid,pid,pname,pcompany,pimg,pprice;
	
	double tprice,total;
	public  ProgressDialog proDialog;
	public  void startLoading() {
	    proDialog = new ProgressDialog(this);
	    proDialog.setMessage("Loading. Please Wait.....");
	  
	    proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	    proDialog.setCancelable(false);
	    proDialog.show();
	}

public  void stopLoading() {
	    proDialog.dismiss();
	    proDialog = null;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenHeight = (int) (metrics.heightPixels * 0.80);

		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_add_cart);
		
        getWindow().setLayout(LayoutParams.WRAP_CONTENT, screenHeight); //set below the setContentview
		
		 ApplicationClass appState = ((ApplicationClass)getApplicationContext());
	    	
			pref=appState.pref;
			editor=appState.editor;
			
		  txt_prodname = (TextView)findViewById(R.id.txt_addcart_prodname);
          txt_companyname = (TextView)findViewById(R.id.txt_addcart_companyname);
          txt_price = (TextView)findViewById(R.id.txt_addcart_price);
          txt_qty=(TextView)findViewById(R.id.txt_addcart_qty);
          btn_addtocart=(Button)findViewById(R.id.btn_addcart_addtocart);
          img_prod=(ImageView)findViewById(R.id.img_addcart_prodimg);
          imgbtn_close=(ImageButton) findViewById(R.id.imgbtn_addcart_close);
          
         addqty=(Button)findViewById(R.id.btn_addcart_addqty);
         subqty=(Button)findViewById(R.id.btn_addcart_subtractqty);
         
//         i.putExtra("pid", json.get("prodid").getAsString());
//			i.putExtra("pname", json.get("description").getAsString());
//			i.putExtra("pcompany",json.get("prodname").getAsString());
//			i.putExtra("pimg", json.get("prodname").getAsString());
//			i.putExtra("pprice", json.get("pricecode").getAsString());
//			i.putExtra("storeid", storeid);
//			i.putExtra("customerid", customerid);
//			i.putExtra("subcatid", subcatid);
//         
         storeid=getIntent().getStringExtra("storeid");
         customerid=getIntent().getStringExtra("customerid");
         pid=getIntent().getStringExtra("pid");
         
         pcompany=getIntent().getStringExtra("pcompany");
         pimg=getIntent().getStringExtra("pimg");
         pprice=getIntent().getStringExtra("pprice");
         pname=getIntent().getStringExtra("pname");
        subcatid=getIntent().getStringExtra("subcatid");
        
         

        count=1;
        txt_companyname.setText(pcompany);
        txt_price.setText(pprice);
        txt_prodname.setText(pname);
        txt_qty.setText(""+count);
        
        	Log.e("img string", pimg);
        
        Ion.with(img_prod)
        .placeholder(R.drawable.loading_def_product)
        .error(R.drawable.loading_def_product)
             
        .load(pimg);
        String price=pprice.substring(3);
       tprice =Double.parseDouble(price);
        total=tprice;
        try{
         addqty.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				
				count++;
				total=total+tprice;
				txt_qty.setText(""+count);
				txt_price.setText("QR "+total);
			}
		});
         
         
         subqty.setOnClickListener(new OnClickListener() {
 			
 			@Override
 			public void onClick(View v) {
 				// TODO Auto-generated method stub
 				if(count>1){
 				count--;
 				
			
 				total=total-tprice;
 				
				txt_qty.setText(""+count);
				txt_price.setText("QR "+total);
 				}
 			}
 		});
        
          btn_addtocart.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(Utilities_Functions.checkinternet(getApplicationContext())){
					startLoading();
				String urlprodlist="http://egrab.qa/en/webservices/cart.php?prodid="+pid+"&quantity="+count+"&customerid="+customerid;
				Ion.with(getApplicationContext())
				.load(urlprodlist)
				.asJsonObject()
				.setCallback(new FutureCallback<JsonObject>() {
				   @Override
				    public void onCompleted(Exception e, JsonObject result) {
					   stopLoading();
					   if(e==null){
				       boolean status=result.get("success").getAsBoolean();
				       if(status){
				    	
				    	  
				    		 if(pref.contains("noofcart")){
							noofcart=pref.getInt("noofcart", 0);
							 if(!pref.contains(pid))
								 noofcart++;
							editor.putInt("noofcart", noofcart);
							editor.commit();
							
							 editor.putInt(pid, count);
					    	 editor.commit();
						}
						else{
							editor.putInt("noofcart", 1);
							editor.commit();
						}
				    		 if(proDialog!=null)
				    		 
				    		 Toast.makeText(getApplicationContext(), "added to cart", Toast.LENGTH_LONG).show();
								
							    
							finish();
				    	  
				       }
				       else{
				    	   //error dialog here;
				    	   Toast.makeText(getApplicationContext(), "Error while connecting...!", Toast.LENGTH_LONG).show();
							
				       }
					   
					   }
					   else{
						   Toast.makeText(getApplicationContext(), "Error while connecting...!", Toast.LENGTH_LONG).show();
					   }
				    }
				});
				}
				else{
					Toast.makeText(getApplicationContext(), "please check your internet connection", Toast.LENGTH_LONG).show();
				}
				
				
			}
		});
          
          imgbtn_close.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
         
        }
        catch(Exception e)
        {
        	
        }
         
	}
}
