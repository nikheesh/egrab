package com.egrab.app;



import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Share_Activity extends Activity{

	ImageButton img_back;
	Button cancel;
	ApplicationClass appState;
	
	ImageView img_whatsapp,img_facebook,img_email,img_twitter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_share);
		
	
		cancel=(Button)findViewById(R.id.btn_share_cancel);
		img_email=(ImageView)findViewById(R.id.img_share_msg);
		img_facebook=(ImageView)findViewById(R.id.img_share_fb);
		img_twitter=(ImageView)findViewById(R.id.img_share_twitter);
		img_whatsapp=(ImageView)findViewById(R.id.img_share_whatsup);
		
		img_whatsapp.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try{
				Intent sendIntent = new Intent();
				sendIntent.setAction(Intent.ACTION_SEND);
				sendIntent.putExtra(Intent.EXTRA_TEXT, "please download eGrab http://egrab.qa");
				sendIntent.setType("text/plain");
				
				sendIntent.setPackage("com.whatsapp");
				startActivity(sendIntent);
				}
				catch (Exception e){
					Toast.makeText(getApplicationContext(), "You are not installed whats app", Toast.LENGTH_LONG).show();
				}
			}
		});
		
		img_facebook.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try{
					Intent sendIntent = new Intent();
					sendIntent.setAction(Intent.ACTION_SEND);
					sendIntent.putExtra(Intent.EXTRA_TEXT, "http://egrab.qa");
					sendIntent.setType("text/plain");
					
					sendIntent.setPackage("com.facebook.katana");
					startActivity(sendIntent);
					}
					catch (Exception e){
						Toast.makeText(getApplicationContext(), "You are not installed facebook", Toast.LENGTH_LONG).show();
					}
				
			}
		});
		
		img_email.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(Intent.ACTION_SEND);
				i.setType("message/rfc822");
				
				i.putExtra(Intent.EXTRA_SUBJECT, "Welcome to eGrab");
				i.putExtra(Intent.EXTRA_TEXT   , "You can download complete online grocery store from http://egrab.qa");
				try {
				    startActivity(Intent.createChooser(i, "Send mail..."));
				} catch (android.content.ActivityNotFoundException ex) {
				    Toast.makeText(getApplicationContext(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
				}
			}
		});
		img_twitter.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				try{
					Intent sendIntent = new Intent();
					sendIntent.setAction(Intent.ACTION_SEND);
					sendIntent.putExtra(Intent.EXTRA_TEXT, "please download eGrab http://egrab.qa");
					sendIntent.setType("text/plain");
					
					sendIntent.setPackage("com.twitter.android");
					startActivity(sendIntent);
					}
					catch (Exception e){
						Toast.makeText(getApplicationContext(), "You are not installed twitter", Toast.LENGTH_LONG).show();
					}
			}
		});
		
		cancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		img_back=(ImageButton)findViewById(R.id.imgbtn_share_backbtn);
		img_back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		
	}

}
