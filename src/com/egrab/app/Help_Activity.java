package com.egrab.app;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class Help_Activity extends Activity{
	TextView txt_faq,txt_call,txt_aboutus,txt_writeus;
	
	ImageButton img_back;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_help);
		
		txt_faq=(TextView)findViewById(R.id.txt_help_faq);
		txt_aboutus=(TextView)findViewById(R.id.txt_help_aboutus);
		txt_call=(TextView)findViewById(R.id.txt_help_callsupport);
		txt_writeus=(TextView)findViewById(R.id.txt_help_writetous);
		
		
		txt_faq.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			
				Intent i=new Intent(getApplicationContext(),FAQ_Activity.class);
				startActivity(i);
				
			}
		});
		
		txt_aboutus.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {

				Intent i=new Intent(getApplicationContext(),About_Activity.class);
				startActivity(i);
				
				
			}
		});
		
		txt_call.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new AlertDialog.Builder(Help_Activity.this)
			    .setIcon(R.drawable.logo_splash)
			    .setMessage("Call customer support?")
			    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
			        public void onClick(DialogInterface dialog, int which) { 
			        	Intent phoneIntent = new Intent(
								Intent.ACTION_CALL);
						phoneIntent.setData(Uri.parse("tel:+97440193059"));
						startActivity(phoneIntent);

			        }
			     })
			    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
			        public void onClick(DialogInterface dialog, int which) { 
			            // do nothing
			        }
			     })
			    
			     .show();
			}
		});
		
		img_back=(ImageButton)findViewById(R.id.imgbtn_help_backbtn);
		img_back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
	}

}
