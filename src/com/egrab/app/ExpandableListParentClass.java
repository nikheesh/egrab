package com.egrab.app;

import java.util.ArrayList;

public class ExpandableListParentClass {
	 private Category_Class parent;
	    private ArrayList<Category_Class> parentChildren;


	    public ExpandableListParentClass() { 
	    this.parent=new Category_Class();
	    this.parentChildren=new ArrayList<Category_Class>();
	    }
	    public ExpandableListParentClass(Category_Class parent, ArrayList<Category_Class> parentChildren) {
	        this.parent = parent;
	        this.parentChildren = parentChildren;
	    }

	    public Category_Class getParent() {
	        return parent;
	    }

	    public void setParent(Category_Class ctclass) {
	    	
	        this.parent.CategoryId=ctclass.CategoryId;
	        this.parent.Categoryname=ctclass.Categoryname;
	    }

	    public ArrayList<Category_Class> getParentChildren() {
	        return parentChildren;
	    }

	    public void setParentChildren(ArrayList<Category_Class> parentChildren) {
	        this.parentChildren = parentChildren;
	    }
	    
	    public void removeallchilds(){
	    	this.parentChildren.clear();
	    }
	}

